#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

double exp(double a, double b){
    double t(1);
    for(int i = 0;i<b;++i)
        t *= a;
    return t;
}
double n_root_(double num, double n_){
    double x;
    double A(num);
    double dx;
    double eps(10e-6);
    double n(n_);
    x = A * 0.5;
    dx = (A/exp(x,n-1)-x)/n;
    while(dx >= eps || dx <= -eps){
        x = x + dx;
        dx = (A/exp(x,n-1)-x)/n;
    }
   return x;
}

int main(){
	int ite;
	cin>>ite;
	int c=1;
	while(ite--){
		cout<<"Case #"<<c++<<": "<<endl;
		int n,m;
		cin>>n>>m;
		int ar[n];
		for(int i=0;i<n;i++){
			cin>>ar[i];
		}
		while(m--){
			double d=1;
			int t1,t2;
			cin>>t1>>t2;
			for(int i=t1;i<=t2;i++)d*=ar[i];
			cout<<n_root_(d,t2-t1+1)<<endl;
		}
	}	
}
