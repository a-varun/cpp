#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int p[1000], level[1000];
int lca[1000][1000];
vector<vector<int> > nodes;

void compute_lca(int n){
	//parent array stores parent of each node in tree. lca is a 2d array for storing result 
	//init the req positions in lca
	for(int i=0;i<n;i++){
		for(int j=0;1<<j <n;j++ ){
			lca[i][j]=-1;
		}
	}
	for(int i=0;i<n;i++)
		lca[i][0]=p[i];
	for(int j=1; 1<<j <n; j++){
		for(int i=0;i<n;i++){
			if(lca[i][j-1]==-1) continue;
				lca[i][j]=lca[lca[i][j-1]][j-1];
		}
	}
}

int query(int node1, int node2, int n){
	int tmp;
	if(level[node1]<level[node2]){
		tmp = node1;
		node1 = node2;
		node2 = tmp;
	}
	int powfa;
	for(powfa = 1; 1<<powfa <= level[node1]; powfa++);
	powfa--;
	
	for(int i=powfa;i>=0;i--){
		if(level[node1] - (1<<i)>=level[node2])
			node1 = lca[node1][i];
	}
	if(node1==node2)
		return node1;
	for(int i=powfa;i>=0;i--)
		if(lca[node1][i]!=-1 && lca[node1][i]!=lca[node2][i]){
			node1=lca[node1][i];node2=lca[node2][i];
		}
	return p[node1];

}

void dfs(int node,int lvl){
	//node -> current node, level-> array to be populated, vector< VI > -> the parent child relation
	level[node]=lvl;
	for(int j=0;j<nodes[node].size();j++){
		dfs(nodes[node][j],lvl+1);
	}
}

int main(){
	ios::sync_with_stdio(false);
	int ite;
	cin>>ite;
	int nums=1;
	while(ite--){
		cout<<"Case "<<nums++<<":"<<endl;
		int n;
		cin>>n;
		memset(level,0,sizeof(level));
		memset(p,-1,sizeof(p));
		p[0]=-1;
		nodes.clear();
		for(int i=0;i<n;i++){
			vector<int> tmp;
			int nchilds,t;
			cin>>nchilds;
			for(int j=0;j<nchilds;j++){
				cin>>t;
				t-=1;
				p[t] = i;
				tmp.PB(t);
			}
			nodes.PB(tmp);
		}
		dfs(0,0);

		compute_lca(n);
		int q;
		cin>>q;
		while(q--){
			int t1,t2;
			cin>>t1>>t2;
			cout<<query(t1-1,t2-1,n)+1<<endl;
		}
	}
}