#include <bits/stdc++.h>
using namespace std;

#define repu(i, begin, end) for (__typeof(begin) i = (begin) - ((begin) > (end)); i != (end) - ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define repe(i, begin, end) for (__typeof(begin) i = (begin); i != (end) + 1 - 2 * ((begin) > (end)); i += 1 - 2 * ((begin) > (end)))
#define mem(a, x) memset(a, x, sizeof(a))
#define all(a) a.begin(), a.end()
#define count_bits(x) __builtin_popcount(x)
#define count_bitsll(x) __builtin_popcountll(x)
#define least_bits(x) __builtin_ffs(x)
#define least_bitsll(x) __builtin_ffsll(x)
#define most_bits(x) 32 - __builtin_clz(x)
#define most_bitsll(x) 64 - __builtin_clz(x)

vector<string> split(const string &s, char c) {
	vector<string> v;
	stringstream ss(s);
	string x;
	while (getline(ss, x, c)) v.push_back(x);
	return v;
}

#define error(args...) { vector<string> _v = split(#args, ','); err(_v.begin(), args); }

void err(vector<string>::iterator it) {}

template<typename T, typename... Args>
void err(vector<string>::iterator it, T a, Args... args) {
	cerr << it -> substr((*it)[0] == ' ', it -> length()) << " = " << a << '\n';
	err(++it, args...);
}

typedef long long ll;
const int MOD = 1000000007;

template<class T> inline T tmin(T a, T b) {return (a < b) ? a : b;}
template<class T> inline T tmax(T a, T b) {return (a > b) ? a : b;}
template<class T> inline void amax(T &a, T b) {if (b > a) a = b;}
template<class T> inline void amin(T &a, T b) {if (b < a) a = b;}
template<class T> inline T tabs(T a) {return (a > 0) ? a : -a;}
template<class T> T gcd(T a, T b) {while (b != 0) {T c = a; a = b; b = c % b;} return a;}

int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(false);
    int q, id, n;
    cin >> q;
    map<int, int> mp;
    vector<int> tmp;
    repu(i, 0, q) {
    	cin >> id >> n;
    	if (id == 0) {
    		int x = 1;
    		tmp.clear();
    		repu(j, 0, 30) {
    			tmp.push_back(n / x);
    			x <<= 1;
    		}
    		sort(all(tmp));
    		tmp.erase(unique(all(tmp)), tmp.end());
    		repu(j, 0, tmp.size()) mp[tmp[j]]++;
    	}
    	else if (id == 1) {
    		int x = 1;
    		tmp.clear();
    		repu(j, 0, 30) {
    			tmp.push_back(n / x);
    			x <<= 1;
    		}
    		sort(all(tmp));
    		tmp.erase(unique(all(tmp)), tmp.end());
    		repu(j, 0, tmp.size()) mp[tmp[j]]--;	
    	}
    	else {
    		printf("%d\n", mp[n]);
    	}
    }

    return 0;
}
