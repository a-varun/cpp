#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

vector<vector<int> > mp(26);

int mapp[26][26];
int vis[26];

void bfs(int origin, int current){
	if(vis[current]) return;
	vis[current]=1;
	mapp[origin][current]=1;
	for(int i=0;i<mp[current].size();i++)
		bfs(origin,mp[current][i]);
}

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		for(int i=0;i<26;i++) mp[i].clear();
		memset(mapp,0,sizeof(mapp));
		string a,b,temp;
		cin>>a>>b;
		int n,fl=0;
		cin>>n;
		while(n--){
			cin>>temp;
			mp[temp[0]-'a'].push_back(temp[3]-'a');
		}
		if(a.length()!=b.length()){
			cout<<"NO"<<endl;
			continue;
		}
		for(int i=0;i<26;i++){
			mapp[i][i]=1;
			if(mp[i].size()!=0){
				memset(vis,0,sizeof(vis));
				bfs(i,i);
			}
		}
		//for(int i=0;i<26;i++){for(int j=0;j<26;j++)cout<<mapp[i][j]<<' ';cout<<endl;}
		for(int i=0;i<a.length();i++){
			if(mapp[a[i]-'a'][b[i]-'a']){
				continue;
			}
			fl=1;
			break;
		}
		if(fl) cout<<"NO"<<endl;
		else
			cout<<"YES"<<endl;

	}

}