#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
#define pr(p) cout<<p<<endl;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
LL MOD = 1000000007;

#define maxval 100001

inline int next_int()
{
  int n=0;
  char c=getchar();
  while(!('0'<=c && c<='9'))
   c=getchar();
  while('0'<=c && c<='9')
  {
    n=n*10+c-'0';
    c=getchar();
  } 
  return n;
}

long long fact[maxval+8],invfact[maxval+8];

long long pow(long long b, long long p){
	long long powo=b,res=1;
	while(p){
		if(p%2){
			res=(res*powo)%MOD;
		}
		powo=(powo*powo)%MOD;
		p/=2;
	}
	return res;
}


void compute(){
	fact[0]=1;
	invfact[1]=1;
	for(int i=1;i<=maxval;i++){
		fact[i]=(i*fact[i-1])%MOD;
	}
	invfact[maxval]=pow(fact[maxval],MOD-2);
	for(int i=maxval;i>0;i--){
		invfact[i-1]=(invfact[i]*i)%MOD;
	}
}

LL comb(LL n,LL m){
	LL ans= fact[n]*invfact[n-m];
	if(ans>MOD) ans%=MOD;
	ans*=invfact[m];
	if(ans>MOD) ans%=MOD;
	return ans;
}

LL perm(LL n,LL m){
	LL ans= fact[n]*invfact[m];
	if(ans>MOD) ans%=MOD;
	return ans;
}


LL calcloop(LL rtk,LL n){
	LL ts=0;
	ts=comb(n+rtk-1,n);
	/*for(LL i=1;i<=rtk;i++){

		ts+=perm(rtk,i)*comb(n-1,i-1);
		if(ts>MOD) ts%=MOD;
	}*/
	return ts;
}

int main(){
	int ite=next_int();
	compute();
	while(ite--){
		LL n=next_int(),k=next_int();
		
		LL ts=0;
		
		LL rtk=LL(sqrt(k));
		ts+=calcloop(rtk,n);

		LL tv = rtk-1;
		if(tv==0) tv=1;
		for(LL i=rtk+1;i<=k;i++){
			if(i*tv>n)
				tv--;
			ts+=calcloop(tv,n-1);
		}
		cout<<ts<<endl;
	}
}