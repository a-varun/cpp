#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	long long int ite,black=0,red=0;
	scanf("%lld",&ite);
	long long int ar1[32000],ar2[32000];
	long long int m,n,fl=0,p1,p2,ptr1,ptr2;
	getchar();
	char c ;
	while(ite--){
		getchar();
		char c= getchar();
		if(c=='i'){
			fl+=1;
			getchar();
			continue;
		}
		scanf("%lld%lld",&m,&n);
		getchar();
		ptr1=0;ptr2=0;
		while(m>1){
			ar1[ptr1++]=m%2;
			m/=2;
		}
		while(n>1){
			ar2[ptr2++]=n%2;
			n/=2;
		}
		ptr1--;
		ptr2--;
		long long int meetup = 0;
		while(ptr1>=0 && ptr2 >=0 &&ar1[ptr1]==ar2[ptr2]){
			ptr1--;
			ptr2--;
			meetup++;
		}
		black=0;
		red=0;
		ptr1++;
		ptr2++;
		if((meetup&1)==(fl&1)){
			black+=1;
			black+=(ptr1)/2;
			red+=(ptr1)/2+(ptr1)%2;
			black+=(ptr2)/2;
			red+=(ptr2)/2+(ptr2)%2;


		}
		else{
			red+=1;
			red+=(ptr1)/2;
			black+=(ptr1)/2+(ptr1)%2;
			red+=(ptr2)/2;
			black+=(ptr2)/2+(ptr2)%2;

		}
		if(c=='b'){
			printf("%lld\n",black);
		}
		else
			printf("%lld\n",red );
	}
}