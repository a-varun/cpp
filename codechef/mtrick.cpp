#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

long long ar[1001];
int main()
{
long long a,b,c;
int n;
	TCases
	{
		scanf("%d",&n);
		REP(i,n) scanf("%lld",&ar[i]);
		scanf("%lld%lld%lld",&a,&b,&c);
		if(a>=c)a%=c;
		if(b>=c)b%=c;
		REP(i,n)
		{
			if(ar[i]>=c) ar[i]%=c;
		}
		string st;
		cin>>st;
		int fl=0;
		REP(i,n)
		{
			if(st[i]=='R')
			{
				fl=fl+1;
			}
			else if(st[i]=='A')
			{
				REP(i,n)
				{
					ar[i]=ar[i]+a;
					if(ar[i]>=c) ar[i]%=c;
				}
			}
			else if(st[i]=='M')
			{
				REP(i,n)
				{
					ar[i]=ar[i]*b;
					if(ar[i]>=c) ar[i]%=c;
				}
			}
			if(fl%2==0)
				printf("%lld ",ar[i]%c);
			else
				printf("%lld ",ar[n-i-1]%c);

		}
		printf("\n");
	}
}