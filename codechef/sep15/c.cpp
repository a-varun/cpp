#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		int n;
		cin>>n;
		vector<pair<int,pair<int,int> > > vtr;
		int t1,t2,xs=INT_MAX,xb=INT_MIN,ys=INT_MAX,yb=INT_MIN;
		REP(i,n){
			cin>>t1>>t2;
			xs = min(xs,t1);
			xb = max(xb,t1);
			ys = min(ys,t2);
			yb = max(yb,t2);
			vtr.PB(MP(t1,MP(t2,i)));
		}
		int fl=0;
		for(int i=0;i<n;i++){
			t1 = vtr[i].first, t2=vtr[i].second.first;
			if(t1==xs && t2==ys){
				fl=1;
				cout<<"1"<<endl<<vtr[i].second.second+1<<" NE"<<endl;
				break;
			}
			if(t1==xs && t2==yb){
				fl=1;
				cout<<"1"<<endl<<vtr[i].second.second+1<<" SE"<<endl;
				break;
			}

			if(t1==xb && t2==ys){
				fl=1;
				cout<<"1"<<endl<<vtr[i].second.second+1<<" NW"<<endl;
				break;
			}
			if(t1==xb && t2==yb){
				fl=1;
				cout<<"1"<<endl<<vtr[i].second.second+1<<" SW"<<endl;
				break;
			}
		}
		if(fl){
			continue;
		}
		SORT(vtr);
		PII p1 = MP(vtr[0].first, vtr[0].second.first);
		PII p2 = MP(vtr[1].first, vtr[1].second.first);
		if(p1.second >= p2.second){
			cout<<"2"<<endl;
			cout<<vtr[0].second.second+1<<" SE"<<endl;
			cout<<vtr[1].second.second+1<<" NE"<<endl;
		}
		else{
			cout<<"2"<<endl;
			cout<<vtr[0].second.second+1<<" NE"<<endl;
			cout<<vtr[1].second.second+1<<" SE"<<endl;
		}
	}	
}
