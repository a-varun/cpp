#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
long long tree[2005000];
long long updt[2005000][2];
int ar[1000006];
void build(int i, int lo, int hi)
{
	updt[i][0]=updt[i][1]=0;
	if(lo==hi)
	{
		tree[i]=ar[lo];
		return;
	}
	int mid=(lo+hi)/2;
	build(2*i+1,lo,mid);
	build(2*i+2,mid+1,hi);
	tree[i]=tree[2*i+1]+tree[2*i+2];
}


void update()

long long query(int lo, int hi, int x, int y, int i)
{
	if(lo>y||hi<x) return 0;
	if(lo>=x&&hi<=y) return tree[i];
	int mid=(lo+hi)/2;
	return query(lo,mid,x,y,2*i+1)+query(mid+1,hi,x,y,2*i+2);
}

int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)
	{
		scanf("%d",&ar[i]);
	}
	build(0,0,n-1);
	while(m--)
	{
		int x,y;
		cin>>x>>y;
		cout<<query(0,n-1,x,y,0)<<endl;
	}
}