#include <iostream>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <vector>
#include <string.h>
#include <queue>
#include <math.h>
using namespace std;

int nodes[101], gp[101][101];
int n,m;
int gp1[101][101];
int ite;
#define ya printf("YES\n")
#define no printf("NO\n")
bool deletesingle(int a)
{
	int cn=-1;

	for(int i=0;i<n;i++)
	{
		if(gp[a][i]!=0)
		{
			nodes[i]=0;
			gp[a][i]=gp[i][a]=0;
			cn=i;
			nodes[cn]=0;
			break;
		}
	}
	for(int i=0;i<n;i++)
	{
		if(gp[cn][i])
		{
			if(nodes[i]>1)
			{
				nodes[i]--;
				gp[cn][i]=gp[i][cn]=0;
				continue;
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}




bool isSafe(int v,  int path[], int pos)
{

   if (gp1 [ path[pos-1] ][ v ] == 0)
        return false;
 
    for (int i = 0; i < pos; i++)
        if (path[i] == v)
            return false;
 
    return true;
}
 
bool hamCycleUtil(int path[], int pos, int cot)
{
    if (pos == cot)
    {

           return true;
    }
 
    for (int v = 1; v < n; v++)
    {
        if (isSafe(v, path, pos))
        {
            path[pos] = v;
            if (hamCycleUtil ( path, pos+1,cot) == true)
            {
                return true;}
            path[pos] = -1;
        }
    }
    return false;
}
 
bool heyda(int cot)
{
    int *path = new int[n];
    for (int i = 0; i < n; i++)
        path[i] = -1;
 	int cor=-1;
 	for(int i=0;i<n;i++)
 	{
 		for(int j=0;j<n;j++)
 		{
 			if(gp1[i][j]){ cor=i;break;}
 		}
 		if(cor!=-1) break;
 	}
 	path[0]=cor;
    return hamCycleUtil(path, 1, cot);
}







int main()
{
	
	scanf("%d",&ite);
	while(ite--)
	{
		int flip=0;
		scanf("%d%d",&n,&m);
		memset(nodes,0,sizeof(nodes));
		memset(gp,0,sizeof(gp));
		int x,y;
		for(int i=0;i<m;i++)
		{
			scanf("%d%d",&x,&y);
			x--;y--;
			if(!gp[x][y])
			{
				gp[x][y]=1;
				gp[y][x]=1;
				nodes[x]++;
				nodes[y]++;
			}
		}
		if(n==1){no;continue;}
		for(int i=0;i<n;i++)
		{
			if(nodes[i]==0)
			{
				no;
				flip=1;
				break;
			}
		}
		if(flip) continue;
		while(1)
		{
			int i,ct=-1;
			for(i=0;i<n;i++)
			{
				if(nodes[i]==1)
				{
					ct=i;
					break;
				}
			}
			if(ct!=-1)
			{
				nodes[i]=0;
				if(!deletesingle(ct)) {no;flip=1;break;}
			}
			else
				break;
		}

		if(flip) continue;
		int count=0;
		for(int i=0;i<n;i++)
		{
			if(nodes[i]==0)
				continue;
			count++;
		}
		if(!count){ya;continue;}

		if(count%2!=0)
		{
			no;
			continue;
		}
		flip=0;
		while(1)
		{
			memset(gp1,0,sizeof(gp1));
			int cr=-1;
			for(int i=0;i<n;i++)
			{
				for(int j=0;j<n;j++)
				{
					if(gp[i][j]) {cr=i;break;}
				}if(cr!=-1) break;
			}
			if(cr==-1) break;
			queue<int> q;
			q.push(cr);
			int cot=0;
			while(!q.empty())
			{
				int r=q.front();
				q.pop();
				
				for(int j=0;j<n;j++)
				{
					if(gp[r][j])
					{
						cot++;
						gp1[r][j]=gp1[j][r]=1;
						gp[r][j]=gp[j][r]=0;
						q.push(j);
					}
				}
			}
			if(!heyda(cot))
			{
				no;
				flip=1;
				break;
			}
		}
		if(flip) continue;
		else
			ya;
	}
}