#include <iostream>
using namespace std;
#define ll long long
int main(){
	ios::sync_with_stdio(false);	   
     ll ite;
    cin>>ite;
    while(ite--){
        ll n,k,t;
        cin>>n>>k;
        ll ar[n];
        for(ll i=0;i<n;i++){
            cin>>ar[i];
        }
        ll tmax = -1;
        for(ll i=0;i<n;i++){
            cin>>t;
            tmax=max(tmax,((ll)(k/ar[i]))*t);
        }
        cout<<tmax<<endl;
    }
}
