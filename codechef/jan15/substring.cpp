#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;


int tree[3000008][4], ar[100008];
struct node{
	int z, o,t,s;
	node(int q, int w, int e,int r=0){
		z=q;o=w;t=e;s=r;
	}
};

void build(int lo, int hi, int i){
	if(lo==hi){
		tree[i][ar[lo]]=1;
		tree[i][3]=ar[lo];
		return;
	}
	int mid=(hi+lo)/2;
	build(lo,mid,2*i+1);build(mid+1,hi,2*i+2);
	tree[i][0]=tree[2*i+1][0];
	tree[i][1]=tree[2*i+1][1];
	tree[i][2]=tree[2*i+1][2];
	int lsum = (tree[2*i+1][3])%3;
	//cout<<i<<' '<<lsum<<endl;
	//cout<<tree[i][0]<<' '<<tree[i][1]<<' '<<tree[i][2]<<endl;
	tree[i][0]+=tree[2*i+2][(3-lsum)%3];
	tree[i][1]+=tree[2*i+2][(4-lsum)%3];
	tree[i][2]+=tree[2*i+2][(5-lsum)%3];
	//cout<<tree[i][0]<<' '<<tree[i][1]<<' '<<tree[i][2]<<endl;
	lsum+=tree[2*i+2][3];
	tree[i][3]=lsum%3;
}

void update(int lo, int hi, int point, int val, int i){
	if(lo>point||hi<point) return;
	if(lo==hi){
		tree[i][0]=tree[i][1]=tree[i][2]=0;
		tree[i][val]=1;
		tree[i][3]=val;
		return;
	}
	int mid=(hi+lo)/2;
	update(lo,mid,point,val,2*i+1);update(mid+1,hi,point,val,2*i+2);
	tree[i][0]=tree[2*i+1][0];
	tree[i][1]=tree[2*i+1][1];
	tree[i][2]=tree[2*i+1][2];
	int lsum = (tree[2*i+1][3])%3;
	//cout<<i<<' '<<lsum<<endl;
	//cout<<tree[i][0]<<' '<<tree[i][1]<<' '<<tree[i][2]<<endl;
	tree[i][0]+=tree[2*i+2][(3-lsum)%3];
	tree[i][1]+=tree[2*i+2][(4-lsum)%3];
	tree[i][2]+=tree[2*i+2][(5-lsum)%3];
	//cout<<tree[i][0]<<' '<<tree[i][1]<<' '<<tree[i][2]<<endl;
	lsum+=tree[2*i+2][3];
	tree[i][3]=lsum%3;

}

node query(int lo, int hi, int x, int y, int i){
	node tmp(0,0,0);
	if(hi<x||lo>y) {
		return tmp;
	}
	if(lo==hi){
		node temp(tree[i][0],tree[i][1],tree[i][2], tree[i][3]);
		return temp;
	}
	if(lo>=x && hi<=y){
		node temp(tree[i][0],tree[i][1],tree[i][2],tree[i][3]);
		return temp;		
	}
	int mid=(lo+hi)/2;
	node t1 = query(lo,mid,x,y,2*i+1),t2=query(mid+1,hi,x,y,2*i+2);
	node t3(t1.z,t1.o,t1.t);
	int lsum = t1.s%3;
	if(lsum==0){
		//left cumulative sum is 0, so add to zero
		t3.z+=t2.z;
		t3.o+=t2.o;
		t3.t+=t2.t;
	}
	else if(lsum == 1){
		//left cumulative sum is 1. So shift one and add
		t3.z+=t2.t;t3.o+=t2.z;t3.t+=t2.o;
	}
	else{
		//left cumulative sum is 2, so shift 2 and add
		t3.z+=t2.o;t3.o+=t2.t;t3.t+=t2.z;
	}
	lsum+=t2.s;
	t3.s = lsum%3;
	return t3;
}

int main(){
	int n,k,t1,t2,t3;
	cin>>n>>k;
	string s;
	cin>>s;
	for(int i=0;i<s.length();i++){
		ar[i]=(s[i]-'0')%3;
	}
	build(0,n-1,0);
	while(k--){
		cin>>t1>>t2>>t3;
		if(t1==1){ update(0,n-1,t2-1,t3%3,0);continue;}
			node tmp = query(0,n-1,t2-1,t3-1,0);
			long long z=tmp.z+1,o=tmp.o,t=tmp.t;

			//cout<<tmp.z<<' '<<tmp.o<<' '<<tmp.t<<endl;
			long long ans = 0;
			ans+=(z *(z -1))/2;
			ans+=(o *(o-1))/2;
			ans+=(t *(t-1))/2;
			cout<<ans<<endl;
	}
}