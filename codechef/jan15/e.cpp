#include <bits/stdc++.h>
using namespace std;
int myf(long long int a,long long int b){return a<b;}
inline long long sumi(long long n){
	return ((n*(n+1))/2);
}
long long int ar[100009];
int main(){
	int ite;
	cin>>ite;

	while(ite--){
		long long int n,k,s=0,t;
		cin>>n>>k;
		for(int i=0;i<k;i++) cin>>ar[i];
		sort(ar,ar+k,myf);
		if(ar[0]==1){
			cout<<"Chef"<<endl;
			continue;
		}
		if(ar[0]==2){
			cout<<"Mom"<<endl;
			continue;
		}
		long long max_reached = 3, num_considered = 2, value_nobtain = -1, number_omitted, range_begining,range_end;
		for(int i=0;i<k;i++){
			number_omitted = ar[i];
			range_begining = num_considered +1;
			range_end = number_omitted -1;

			if(max_reached < (range_begining-1)){
				value_nobtain = max_reached + 1;
				break;	
			}
			max_reached += sumi(range_end) - sumi(range_begining - 1);
			num_considered = number_omitted;
			//cout<<"Max reached"<< max_reached<<endl;
		}

		if(value_nobtain==-1){
			range_begining = num_considered +1;
			range_end = n;
			if(max_reached < (range_begining-1)){
				value_nobtain = max_reached + 1;
			}
			else{
				max_reached += sumi(range_end) - sumi(range_begining - 1);
				value_nobtain = max_reached+1;
			}
		}
		//cout<<value_nobtain<<endl;
		if(value_nobtain%2==0){
			cout<<"Mom"<<endl;
		}
		else
			cout<<"Chef"<<endl;

	}
}
