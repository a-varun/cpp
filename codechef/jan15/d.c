#include <bits/stdc++.h>
using namespace std;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		vector<pair<int, int> > vtr;
		int n,t1,t2;
		cin>>n;
		for(int i=0;i<n;i++){
			cin>>t1>>t2;
			vtr.push_back(make_pair(t2,t1));
		}
		sort(vtr.begin(),vtr.end());
		int a=vtr[0].first, cnt=1;
		for(int i=1;i<n;i++){
			if((vtr[i].second<=a)&&(vtr[i].first>=a))
				continue;
			a=vtr[i].first;
			cnt+=1;
		}
		cout<<cnt<<endl;
	}
}
