#include <bits/stdc++.h>
using namespace std;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		int n;
		cin>>n;
		int s1=0,s2=0,t,s=0;
		for(int i=0;i<n;i++){
			cin>>t;
			if(t==0){
				continue;
			}
			s+=1;
			s1+=t-1;
			s2+=t;
		}
		if(s1<100 && s2>=100){
			cout<<"YES"<<endl;
		}
		else
			cout<<"NO"<<endl;

	}
}
