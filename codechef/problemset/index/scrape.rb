require 'open-uri'
require 'nokogiri'

page = Nokogiri::HTML(open("http://codeforces.com/contest/318/submission/3900765"))
f1 = File.open("inputcf", "w")
f2 = File.open("outputcf", "w")
cnt = 0
count = page.css(".answer-view pre , .input-view pre").count / 2
f1.write(count.to_s + "\n")
page.css(".answer-view pre , .input-view pre").each do |item|
	if cnt.even?
		b = item.text.split.join(" ")

		f1.write(b + "\n")
	else
		res = item.text.split.join(" ")
		f2.write(res + "\n")
	end
	puts item.text
	cnt += 1
end 