#include <cmath>
#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <algorithm>

using namespace std;

#define foreach(e,x) for(__typeof((x).begin()) e=(x).begin(); e!=(x).end(); ++e)

const int N = 100000 + 10;

void solve()
{
	string a;
	cin >> a;
	if (a.size() <= 10) {
		cout << "NO" << endl;
		return;
	}
	int tmp = a.size() - 10;
	for(int i = 0; i + tmp <= a.size(); ++ i) {
		if (a.substr(0, i) + a.substr(i + tmp) == "CODEFORCES") {
			cout << "YES" << endl;
			return;
		}
	}
	cout << "NO" << endl;
}

int main()
{
	solve();
	return 0;
}