#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		string s,s1;
		cin>>s>>s1;
		if(s1.length()>=s.length()){
			cout<<"NO"<<endl;
			continue;
		}
		int n = s.length();
		int frnt = 0;
		REP(i,n){
			if(i>=s1.length()) break;
			if(s[i]==s1[i]) frnt++;
			else
				break;
		}
		int back = s1.length()-1;
		for(int i=n-1;i>=0;i--){
			if(back<0) break;
			if(s[i]==s1[back])back--;
			else
				break;
		}
		
		if(back<frnt)
			cout<<"YES"<<endl;
		else
			cout<<"NO"<<endl;
	}	
}
