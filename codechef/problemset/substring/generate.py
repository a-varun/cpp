import sys,random,string;
filen = sys.argv[1]
f = open(filen, "w")
fout = open(filen + ".out","w")
tcas = 100
f.write(str(tcas)+"\n")
while tcas:
	tcas-=1
	ite = 100000

	stri = ''.join(random.choice(string.ascii_uppercase.replace('x','')) for _ in range(ite))
	slen = random.randint(1,len(stri)-1)
	p = random.randint(0,len(stri)-slen)
	stri2 = stri[:p]+stri[p+slen:]
	stri2 = list(stri2)
	stri2[len(stri2)/2]='x'
	stri2 = "".join(stri2)
	f.write(stri+"\n"+stri2+"\n")
	fout.write("NO"+"\n")
f.close()
fout.close()
