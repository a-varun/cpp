require 'open-uri'
require 'nokogiri'

page = Nokogiri::HTML(open("http://codeforces.com/contest/538/submission/13136174"))
f1 = File.open("inputcf", "w")
f2 = File.open("outputcf", "w")
cnt = 0
count = page.css(".answer-view pre , .input-view pre").count / 2
f1.write(count.to_s + "\n")
page.css(".answer-view pre , .input-view pre").each do |item|
	if cnt.even?
		b = item.text.split.join(" ")

		f1.write(b + "\n" + "CODEFORCES" + "\n")
	else
		res = item.text.split.join(" ")
		if res == "YES"
			f2.write("Understood" + "\n")
		else
			f2.write("Misunderstood" + "\n")
		end
	end
	puts item.text
	cnt += 1
end 