Karthik Kamal started playing Clash of Clans hardcore. He just moved to TH8, and started rebuilding his base.

In coc, there will be a nxn field where you build your buildings the major building would be the walls. For building it, the player would have to place a wall stem, and it would get joined with the stems in its surrounding cells ( 8 directions).


There is a special troop called wall breaker, which is an expert in breaking walls. However, it breaks only the walls of a special type. After deploying, the wall breaker will choose the triple of wall, which forms a right angle. For example, in the above diagram (a), the wall breaker will choose the stem a,b,c as it forms a right angle, and burst there. 

It is your job, given the grid of walls, to find the number of possible locations where the wall breaker can burst. You can see that in example 2, the wall breaker cannot burst, as no right angle is formed.

Input:
The input consists of T, the number of test cases, followed by T test cases.
Each test case begins with n, the length/bredth of the grid. Then n strings follow, defining the grid ('.' - empty space, 'x' - wall stem).

Input constraints:
1<=T<=100
1<=n<=100

Sample:
2
5
xxxxx
x...x
x.xxx
x...x
xxxxx
3
xx.
xx.
...