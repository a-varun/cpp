#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
vector<string> vtr;
int n;

int dir[8][2]={{-1,-1},{-1,0},{-1,1},{0,1},{1,1},{1,0},{1,-1},{0,-1}};

int check(PII y1, PII y2, PII n1){
	if(y1.FST < 0 || y1.FST >= n || y1.SEC < 0 || y1.SEC >= n || vtr[y1.FST][y1.SEC]!='x'){
		return 0;
	}
	if(y2.FST < 0 || y2.FST >= n || y2.SEC < 0 || y2.SEC >= n || vtr[y2.FST][y2.SEC]!='x'){
		return 0;
	}
	if(n1.FST < 0 || n1.FST >= n || n1.SEC < 0 || n1.SEC >= n || vtr[n1.FST][n1.SEC]=='x'){
		return 0;
	}
	return 1;
}
int ch2(int x, int y){
	int tot=0;
	for(int i=0;i<8;i++){
		int j,k;
		j=(i+1)%8;
		k=(i+2)%8;
		tot+= check(MP(x+dir[i][0], y+dir[i][1]),MP(x+dir[k][0], y+dir[k][1]),MP(x+dir[j][0], y+dir[j][1]));
	}
	return tot; 
}
int main(){
	int ite;
	cin>>ite;
	while(ite--){
		vtr.clear();
		cin>>n;
		string s;
		REP(i,n){
			cin>>s;
			vtr.PB(s);
		}
		int tot = 0;
		REP(i,n){
			REP(j,n){
				if(vtr[i][j]=='x'){

					tot += ch2(i,j);
				}
			}
		}
		cout<<tot<<endl;

	}	
}
