#include <bits/stdc++.h>
using namespace std;
#define PB push_back
#define MP make_pair
#define FST first
#define SEC second
#define SZ(a) (int)(a.size())
#define CLR(a) a.clear()
#define SET(a,b) memset(a,b,sizeof(a))
#define LET(x,a) __typeof(a) x(a)
#define TR(v,it) for( LET(it,v.begin()) ; it != v.end() ; it++)
#define REP(i,n) for(int i=0; i<(int)n;i++)
#define si(n) scanf("%d",&n)
#define sll(n) scanf("%lld",&n)
#define pi(n) printf("%d",n)
#define piw(n) printf("%d ",n)
#define pin(n) printf("%d\n",n)
#define SORT(a) sort(a.begin(),a.end())
#define all(a) a.begin(),a.end()
#define endl "\n"

typedef long long LL;
typedef pair<int,int> PII;
typedef vector<int> VI;
typedef vector< PII > VPII;

string s[1000];
int n;

char getit(int x, int y) {
	if(x<n && y<n && x>=0 && y>=0) {
		return s[x][y] == 'x';
	}	
	return false;
}

int main()
{
	ios::sync_with_stdio(false);
	//cin.tie(NULL);
	int ite;
	cin >> ite;
	while(ite--) {
		cin >> n;
		REP(i, n) {
			cin >> s[i];
		}
		int res = 0;
		REP(i, n) {
			REP(j, n) {
				if(s[i][j] == '.') continue;
				if(getit(i-1, j) && getit(i, j-1) && !getit(i-1, j-1)) {
					res += 1;
				}
				if(getit(i, j-1) && getit(i+1, j) && !getit(i+1, j-1)) {
					res += 1;
				}
				if(getit(i+1, j) && getit(i, j+1) && !getit(i+1, j+1)) {
					res += 1;
				}
				if(getit(i, j+1) && getit(i-1, j) && !getit(i-1, j+1)) {
					res += 1;
				}
				if(getit(i-1, j+1) && getit(i-1, j-1) && !getit(i-1, j)) {
					res += 1;
				}
				if(getit(i-1, j-1) && getit(i+1, j-1) && !getit(i, j-1)) {
					res += 1;
				}
				if(getit(i+1, j-1) && getit(i+1, j+1) && !getit(i+1, j)) {
					res += 1;
				}
				if(getit(i+1, j+1) && getit(i-1, j+1) && !getit(i, j+1)) {
					res += 1;
				}
			}
		}
		cout << res << endl;
	}	    
    return 0;
}
