#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

string s;

int ispalin(int b, int e){
	while(b<e){
		if(s[b++]==s[e--])
			continue;
		return 0;
	}
	return 1;

}


int main(){
	int ite;
	cin>>ite;
	while(ite--){
		cin>>s;
		int b=0,e=s.length()-1,fl=-1;

		while(b<e){
			if(s[b]==s[e]){
				b++;e--;continue;
			}
			fl=0;
			if(s[b+1]==s[e])
				if(ispalin(b+1,e)){
					fl=1;
					break;
				}
			if(s[b]==s[e-1])
				if(ispalin(b,e-1)){
					fl=1;
					break;
				}
			break;
		}
		if(abs(fl)) cout<<"YES"<<endl;
		else
			cout<<"NO"<<endl;


	}

}