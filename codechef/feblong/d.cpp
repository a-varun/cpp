#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

// #define TRACE

#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;

#else

#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
#endif
// #define getchar_unlocked getchar
// #define putchar_unlocked putchar

string ip(){string i="";int temp=getchar_unlocked();while(temp<'a'||temp>'z')temp=getchar_unlocked();while(temp>='a'&&temp<='z'){i+=(char)temp;temp=getchar_unlocked();}return i;}



LL scan_d()
{
    LL ip = getchar_unlocked(), ret = 0, flag = 1;

    for(; ip < '0' || ip > '9'; ip = getchar_unlocked())
    {
        if(ip == '-')
        {
            flag = -1;
            ip = getchar_unlocked();
            break;
        }
    }

    for(; ip >= '0'&& ip <= '9'; ip = getchar_unlocked())
        ret = ret * 10 + ip - '0';
    return flag * ret;
}

LL count1[1000004][4];
LL dp[1000004][4][4];
char s[1000007];

int main(){
	// ios::sync_with_stdio(false);
	string s;
	s=ip();
	// cout<<p<<endl;
	trace1(s);
	int p,p1;
	int n=s.length();
	for(int i=n-1;i>=0;i--){
		switch(s[i]){
			case 'c':p=0;break;
			case 'h':p=1;break;
			case 'e':p=2;break;
			case 'f':p=3;break;
		}
		count1[i][p]+=1;
		count1[i][0]+=count1[i+1][0];
		count1[i][1]+=count1[i+1][1];
		count1[i][2]+=count1[i+1][2];
		count1[i][3]+=count1[i+1][3];

		REP(l,4)REP(m,4)if(l!=m)
			dp[i][l][m]=dp[i+1][l][m];
		REP(j,4){
			dp[i][p][j]+=count1[i][j];
		}
	}
	LL ite;
	ite=scan_d();
	// REP(i,n){REP(j,4){REP(k,4)cout<<dp[i][j][k];cout<<endl;}cout<<endl;}
	// getchar();
	while(ite--){
		char si,d;
		LL l1,l2;
		scanf("%c",&si);
		getchar();
		scanf("%c",&d);
		l1=scan_d();
		l2=scan_d();
		trace4(si,d,l1,l2);
		// getchar();
		switch(si){
			case 'c':p=0;break;
			case 'h':p=1;break;
			case 'e':p=2;break;
			case 'f':p=3;break;
		}
		switch(d){
			case 'c':p1=0;break;
			case 'h':p1=1;break;
			case 'e':p1=2;break;
			case 'f':p1=3;break;
		}
		LL t=count1[l1-1][p] - count1[l2-1][p], t1 = count1[l2][p1];
		// cout<<t<<' '<<t1<<endl;
		LL s=t*t1;
		// cout<<s<<endl;
		write(dp[l1-1][p][p1]-dp[l2-1][p][p1] -s);
	}
}