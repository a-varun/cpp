#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

long long ans[10000007];
long long n,m;
inline void prec(){
	LL fact=1;
	LL tmp;
	for(int i=1;i<=m;i++){
		fact*=i;
		if(fact>=m) fact%=m;
		ans[i]=(fact*i)%m;
	}
	fact=0;
	for(int i=1;i<=m;i++){
		fact+=ans[i];
		ans[i]=fact;
	}
}

int main(){
	cin>>n>>m;
	prec();
	long long com=0;
	long long t;
	while(n--){
		cin>>t;
		LL p=t;
		if(p>m) p=m;
		t%=m;
		com=(com+( ans[p] + ((((t*(t+1LL))/2LL)%m)*t)%m))%m;
	}
	cout<<com<<endl;
}