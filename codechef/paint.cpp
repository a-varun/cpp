#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
int main(){
	int ite;
	scanf("%d",&ite);
	while(ite--){
		int n,m,t1,t2,color,weight;
		scanf("%d%d",&n,&m);
		vector<pair<int, int> > vtr;
		for(int i=0;i<m;i++){
			scanf("%d%d",&t1,&t2);
			vtr.push_back(make_pair(t2-1,t1));
		}
		sort(vtr.begin(),vtr.end());
		long long p1c=0, p1count=0, p2c, p2count=0, sum=0;
		p1c=vtr[0].first;

		for(int i=1;i<vtr.size();i++){
			color=vtr[i].second;
			weight=vtr[i].first;
			cout<<color<<' '<<p1c<<endl;
			if(p1c!=color){
				sum+=(p2count)*(p1count);
				p2c=p1c;
				p2count=p1count;
				p1c=color;
				i-=1;
				continue;

			}
			else{
				p1count+=weight - vtr[i-1].first;
			}
		}
		sum+=(p2count)*(p1count);
		printf("%lld\n",sum);
	}
}