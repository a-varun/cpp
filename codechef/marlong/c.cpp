#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;


#define TRACE

#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
#define getchar_unlocked getchar
#define putchar_unlocked putchar

#else

#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
#endif



int main(){
	int ite;
	cin>>ite;
	while(ite--){
		int n,k,q;
		cin>>n>>k>>q;
		string s;
		cin>>s;
		int start[n+10], end[n+10];
		memset(start,0,sizeof(start));
		memset(end,0,sizeof(end));
		int ptr1=0, ptr2=0;
		int zc = s[0]=='0';
		int lp = 0, rp = 1;
		while(lp<n&&rp<n){
			// trace1(s[rp]);
			int tz = zc + (s[rp]=='0'?1:0), to = (rp-lp)-zc+(s[rp]=='1'?1:0);
			// trace5(lp,rp,zc,tz,to);
			if(tz<=k && to<=k){
				zc = tz;
				rp+=1;
				continue;
			}
			//here, it is greater than k.dp[lp]must be closed. It should not include rp
			start[lp]=rp-lp;
			zc-=s[lp]=='0';
			lp++;
			continue;
		}
		while(lp<n){
			start[lp]=n-lp;
			lp++;
		}
		for(int i=0;i<n;i++) cout<<start[i]<<' ';cout<<endl;
		for(int i=1;i<n;i++) start[i]+=start[i-1];

		zc = s[n-1]=='0';
		lp = n-1, rp = n-2;
		while(lp>=0&&rp>=0){
			// trace1(s[rp]);
			int tz = zc + (s[rp]=='0'?1:0), to = (lp-rp)-zc+(s[rp]=='1'?1:0);
			// trace5(lp,rp,zc,tz,to);
			if(tz<=k && to<=k){
				zc = tz;
				rp-=1;
				continue;
			}
			//here, it is greater than k.dp[lp]must be closed. It should not include rp
			end[lp]=lp-rp;
			zc-=s[lp]=='0';
			lp--;
			continue;
		}
		while(lp>=0){
			end[lp]=lp+1;
			lp--;
		}
		for(int i=0;i<n;i++) cout<<end[i]<<' ';cout<<endl;
		for(int i=1;i<n;i++) end[i]+=end[i-1];
		for(int i=n;i>0;i--) end[i]=end[i-1];
		for(int i=n;i>0;i--) start[i]=start[i-1];
		end[0]=start[0]=0;

		while(q--){
			int q,w;
			cin>>q>>w;
			int sl = start[q-1], sm = start[w]-start[q-1], sr = start[n-1]-start[w];
			int el = end[q-1], em = end[w]-end[q-1], er = end[n-1]-end[w];
			int elr = er-sr;
			int sll = srl-el;
			int slm = sm - em;
			


		}
	}
}