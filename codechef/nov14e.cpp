#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	int ite;
	scanf("%d",&ite);
	getchar();
	char t=0;
	int n,m,fl=0,red,black,k,l,lvl;
	char ar1[32000],ar2[32000];
	while(ite--){
		getchar();
		t=getchar();
		if(t=='i'){
			fl++;
			getchar();
			continue;
		}
		scanf("%d%d",&n,&m);
		getchar();
		int ptr1=-1,ptr2=-1;
		while(n>1){
			ar1[++ptr1]= (n&1)+'0';
			n/=2;
		}

		while(m>1){
			ar2[++ptr2]= (m&1)+'0';
			m/=2;
		}

		red=0;black=0;
		k=ptr1;l=ptr2;
		while(ptr1>=0 && ptr2 >= 0 && ar1[ptr1]==ar2[ptr2]){
			ptr1--;
			ptr2--;
		}
		if(ptr1==-1 && ptr2==-1){
			if((k&1)==(fl&1))
				red++;
			else
				black++;
		}
		else if(ptr1== -1){
			lvl = l-ptr2;
			cout<<"level"<<lvl<<' '<<l<<' '<<k<<endl;
			if(k==-1){
				if(fl&1){
					red=1;
					black += (l)/2+ (l)%2;
					red += (l)/2;
				}
				else{
					black=1;
					black += (l)/2 ;
					red += (l)/2+ (l)%2;					
				}
			}
			else if(((lvl)&1)==(fl&1)){
				black=1;
				black += (l-k)/2 + (l-k)%2;
				red += (l-k)/2;
			}
			else{
				red=1;
				black += (l-k)/2;
				red += (l-k)/2+ (l-k)%2;			}
		}
		cout<<black<<' '<<red<<endl;
	}
}