int main()
{
	int n,temp;
	scanf("%d",&n);
	vector<int> nums;
	REP(i,n){
		scanf("%d",&temp);
		nums.PB(temp);
	}
	int ar[n][n];
	memset(ar,-1,sizeof(ar));
	for(int i=0;i<n;i++)
		ar[i][i]=nums[i];

	for(int i=0;i<n;i++)
	{
		for(int j=i+1;j<n;j++)
		{
			ar[i][j]=min(ar[i][j-1],nums[j]);
		}
	}
	int ite;
	scanf("%d",&ite);
	while(ite--){
		int num,count=0;
		scanf("%d",&num);
		for(int i=0;i<n;i++)
		 for(int j=0;j<n;j++)
		  if(ar[i][j]==num)
		   count++;
		printf("%d\n",count);
	}

}