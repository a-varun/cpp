#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#ifdef _WIN32
    #define gc getchar
    #define pc putchar
#else
    #define gc getchar_unlocked
    #define pc putchar_unlocked
#endif
 
#define li long long int

inline void fast_read(int* input) {
    int c = 0;
    while(c < 33) {
        c = gc();
    }
 
    *input = 0;
 
    while(c > 33) {
        *input = *input * 10 + c  - '0';
        c = gc();
    }
}

int main()
{
	TCases
	{
		int n,s;
		fast_read(&n);
		fast_read(&s);
		priority_queue< pair<int,int> > leaves;
		vector< pair<int, pair<int, int> > >pq;
		map<int,bool> seats;
		int t1,t2,t3;
		
		while(n--)
		{
			fast_read(&t1);
			fast_read(&t2);
			fast_read(&t3);
			pq.push_back( make_pair(t3-1, make_pair( t2, t1) ) );
		}
		sort(ALL(pq));
		int count=0;
		for(int i=0;i<pq.size();i++)
		{

			while(!leaves.empty()&& leaves.top().FST*-1 <= pq[i].SEC.SEC )
			{
				seats[leaves.top().SEC]=0;
				leaves.pop();
			}
			if(seats[pq[i].FST]==0)
			{
				seats[pq[i].FST]=1;
				leaves.push(make_pair( pq[i].SEC.FST*-1, pq[i].FST));
				count+=1;
			}

		}
		cout<<count<<endl;
	}
}