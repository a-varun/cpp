#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	int ite;
	cin>>ite;
	while(ite--){
		string s;
		cin>>s;
		int n;
		cin>>n;
		int c=0;
		REP(i,s.length()){
			if(s[i]=='T') c+=2;
			else
				c+=1;
		}
		n*=12;
		if(c>n){
			cout<<"0"<<endl;
			continue;
		}
		// //ans is n-k+1 pl n-2k+1 pl n-3k+1.... n-sk+1
		// //sn -sk+s
		int s1=n/c - (n%c==0?1:0);
		// 36 - 12 + 1 + 36 -24 + 1 + 36 - 36 + 1
		int ans= (s1*n) - (s1*c) ;
		// int ans=0;
		// for(int j=c;j<=n;j+=c){
		// 	int p=0;
		// 	for(int i=1;i<=n;i++){
		// 		if((i+j)<=n){
		// 			ans++;p++;
		// 		}
		// 	}
		// 	cout<<j<<' '<<p<<endl;
		// }
		cout<<ans<<endl;
	}
}