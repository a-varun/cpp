#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int totprimes;
#define int long long
int primes[100000],smthn[100009];
int seive(){
	smthn[0]=smthn[1]=1;
	for(int i=2;i*i<100009;i++){
		if(smthn[i]) continue;
		for(int j=i+i;j<100009;j+=i)
			smthn[j]=1;
	}
	int ptr=0;
	for(int i=2;i<100009;i++){
		if(!smthn[i])
			primes[ptr++]=i;
	}
	totprimes=ptr;
}

#undef int
int main(){
	ios::sync_with_stdio(false);
	#define int long long
	seive();
	int ite;
	cin>>ite;
	while(ite--){
		int n;
		cin>>n;
		int ptr=0;
		int ans=1;
		while(n>1){
			int c=0;
			if(n%primes[ptr]==0){
				int ts =1, k=1;
				while(n%primes[ptr]==0){
					n/=primes[ptr];
					k*=primes[ptr];
					ts+= k;
				}
				ans*=ts;
			}
			ptr++;
			if(ptr>=totprimes){
				ans=ans*(n+1);
				break;
			}
		}
		cout<<ans<<endl;
	}
}