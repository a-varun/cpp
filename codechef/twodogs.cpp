#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>

using namespace std;

#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))

#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

using namespace std;

//Powered by [KawigiEdit] 2.0!

int main()
{
	int n,sum;
	scanf("%d%d",&n,&sum);
	vector< pair< int, int> > ar;
	int temp;

	for(int i=0;i<n;i++)
	{
		scanf("%d",&temp);
		ar.PB(make_pair(temp,i));
	}
	sort(ar.begin(),ar.end());
	int ptr1=0,ptr2=n-1,su=-1;
	while(ptr1<ptr2)
	{
		int p=ar[ptr1].first+ar[ptr2].first;
		if(p==sum)
		{
			int s1=min(ar[ptr1].second+1,n-ar[ptr1].second);
			int s2=min(ar[ptr2].second+1,n-ar[ptr2].second);
			if(su==-1){
				su=max(s1,s2);
			}
			else
			{
				su=min(max(s1,s2),su);
			}
			ptr2--;
		}
		else if(p<sum)
		{
			ptr1++;
		}
		else if(p>sum)
		{
			ptr2--;
		}
	}
	cout<<su<<endl;
}