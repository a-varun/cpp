#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>

using namespace std;

#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))

#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

using namespace std;

int ar[1000001];
int dp[1000001][2];
int recurse(int j,int i, int n)
{
	if(i==n) return 0;
//	if(dp[i]!=-1)return dp[i];
	if(j<ar[i])
		return dp[i][0]=recurse(ar[i],i+1,n);
	if(j==ar[i]) {ar[i]=ar[i]+1;return dp[i][1]=recurse(ar[i],i+1,n)+1;}
	return dp[i][1]=min(recurse(ar[i-1]+1,i+1,n)+1,recurse(ar[i],i+1,n));
}



int main()
{
	int ite;
	scanf("%d",&ite);
	while(ite--)
	{
		int m,n,sum=0;
		scanf("%d%d",&m,&n);
		for(int i=1;i<=m;i++)
		{
			sum+=pow(2,i);
			if(sum==n)
				sum-=1;
		}
		cout<<sum<<endl;
	}

}
