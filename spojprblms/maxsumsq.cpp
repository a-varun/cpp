#include <bits/stdc++.h>
using namespace std;
long long ar[100001];
int main(){
  long long ite;
  cin>>ite;
  while (ite--)
    {
    	long long n;
	 	cin>>n;
	 	for(long long i=0;i<n;i++)
	 		cin>>ar[i];
	 	long long maxi=-10005,sum=0;
	 	for(long long i=0;i<n;i++){
	 		sum+=ar[i];
	 		if(maxi<sum)maxi=sum;
	 		if(sum<0) sum=0;
	 	}
	 	sum=0;
	 	long long t=0,r=0;
	 	for(long long i=0;i<n;i++){
	 		sum+=ar[i];
	 		if(sum==maxi)
	 			r+=1+t;
	 		if(sum==0) t++;
	 		if(sum<0){
	 			sum=0;
	 			t=0;
	 		}
	 	}
	 	printf("%lld %lld\n",maxi,r);
    }
}
