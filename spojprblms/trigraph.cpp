#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main()
{
	long long int n,ite=1;
	scanf("%lld",&n);
	while(n)
	{
		long long ar[n][3];
		for(long long int i=0;i<n;i++)
		{
			scanf("%lld%lld%lld",&ar[i][0],&ar[i][1],&ar[i][2]);
		}
		ar[n-1][0]=min(ar[n-1][0],ar[n-1][0]+ar[n-1][1]);
		if(n==2)
		{
			ar[0][1]+=min(ar[1][0],min(ar[0][2]+ar[1][1],ar[1][1]));
			printf("%lld. %lld\n",ite++,ar[0][1]);
			scanf("%lld",&n);
			continue;
		}
		ar[n-2][2]+=ar[n-1][1];
		ar[n-2][1]+=min(ar[n-1][1],min(ar[n-1][0],ar[n-2][2]));
		ar[n-2][0]+=min(ar[n-1][1],min(ar[n-2][1],ar[n-1][0]));
		for(long long int i=n-3;i>0;i--)
		{
			ar[i][2]+=min(ar[i+1][2],ar[i+1][1]);
			ar[i][1]+=min(ar[i+1][0],min(ar[i+1][1],min(ar[i+1][2],ar[i][2])));
			ar[i][0]+=min(ar[i+1][0],min(ar[i+1][1],ar[i][1]));
		}
		ar[0][2]+=min(ar[1][1],ar[1][2]);
		ar[0][1]+=min(ar[1][0],min(ar[1][1],min(ar[0][2],ar[1][2])));
		printf("%lld. %lld\n",ite++,ar[0][1]);
		scanf("%lld",&n);
	}	
}