#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <stdio.h>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
#define FL(i) cout<<"In "<<i<<endl;
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int p;

struct link
{   
    int start;
    int data;
    link *next;
    link *prev;
    link(int st,int val,link *a,link *b)
    {
        start=st;
        data=val;
        next=a;
        prev=b;
    }
};
 
class text_to_BF
{
    struct link *head;
    int a[20];
public:
    text_to_BF()
    {
        head=NULL;
        for(int i=0;i<21;i++)
            a[i]=0;
    }
    void tokenize(string s);
    void brainfAssign(string s);
    void printBrainfc(string s);
    string get_text();
};

//This function will get the input string

string text_to_BF::get_text()
{
    string s,a;
    getline(cin,a);
    while(a!="")
    {
        s+=a;
        s+='\n';
        getline(cin,a);
    }
    return s;
}


//This function will tokenize the text

void text_to_BF::tokenize(string s)
{
    for(int i=0;i<s.length();i++)
    {
        a[(int)s[i]/10]++;
    }
    brainfAssign(s);
}

//This function will initialize the list.

void text_to_BF::brainfAssign(string s)
{
    for(int i=0;i<20;i++)
    {
        if(a[i]==0)continue;
        if(!head)
        {
            head=new struct link(i*10,i*10,NULL,NULL);
            continue;
        }
        struct link *t=head;
        while(t->next)
            t=t->next;
        t->next = new struct link(i*10,i*10,NULL,t);
    }
    printBrainfc(s);
}

//This function will print the BF code for the corresponding text.

void text_to_BF::printBrainfc(string s)   
{
    int size=0;
    struct link *t=head;
    while(t)
    {
        size++;
        t=t->next;
    }
    for(int i=0;i<10;i++)
        cout<<"+";
    cout<<endl;
    cout<<"["<<endl;
    t=head;
    int i=1;
    while(t)
    {
        cout<<">";
        for(int j=0;j<t->data/10;j++)
        cout<<"+";
        cout<<endl;
        t=t->next;
    }
    for(i=0;i<size;i++)
        cout<<"<";
    cout<<"-]>"<<endl;
    t=head;
    if(t!=NULL)
    {
        for(int i=0;i<s.length();i++)
        {
            while(t && (t->start < ((int)s[i]/10)*10))
            {
                cout<<">";
                t=t->next;
            }
            while(t && (t->start > ((int)s[i]/10)*10))
            {
                cout<<"<";
                t=t->prev;
            }
            if(t->start == ((int)s[i]/10)*10)
            {
                if(t->data < (int)s[i])
                {
                    for(int z=t->data;z<(int)s[i];z++)
                    {
                        cout<<"+";
                    }
                    cout<<"."<<endl;
                    t->data=(int)s[i];
                    continue;
                }
                else if(t->data > (int)s[i])
                {
                    for(int z=t->data;z>(int)s[i];z--)
                    {
                        cout<<"-";
                    }
                    cout<<"."<<endl;
                    t->data=(int)s[i];
                    continue;
                }
                else
                {
                    cout<<".";
                }
            }
        }
    }
}


int main()
{
          text_to_BF a;
           string s;
           s=a.get_text();
           a.tokenize(s);
    
}