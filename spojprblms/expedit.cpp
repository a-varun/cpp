#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;


int main(){
	int ite;
	cin>>ite;
	while(ite--){
		int n,p,l;
		cin>>n;
		priority_queue<pair<int, int> > pq;
		int ar[n][2];
		for(int i=0;i<n;i++){
			cin>>l>>p;
			ar[i][0]=l;ar[i][1]=p;
		}

		cin>>l>>p;
		for(int i=0;i<n;i++){
			pq.push(MP(-(l-ar[i][0]),ar[i][1]));
		}

		int epos=l;
		l=p;
		int count=0;
		int tmax=0;
		while(!pq.empty() && l<epos){
			int sr=0, fl=0;
			while(!pq.empty() && (-pq.top().first) <=l){
				fl=1;
				sr=max(sr,  );
				pq.pop();
			}
			count+=1;
			l+=sr;
		}
		if(l<epos) cout<<"-1"<<endl;
		else
			cout<<count<<endl;
	}
}