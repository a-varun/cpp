#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;


string str;
int num;
void recurse(int sum,int len)
{
	if(len==str.length())
	{
		num++;
		return;
	}
	int i, ts=0;	
	for(i=len;i<str.length();i++)
	{
		if((ts*10+str[i]-'0')>=sum) break;
		ts=ts*10+str[i]-'0';
	}
	cout<<i<<endl;
	for(int j=i;j<str.length();j++)
	{
		ts=ts*10+str[i]-'0';
		recurse(ts,i+1);
	}
}
int main()
{
	
	for(int ite=1;;ite++)
	{
		num=0;
		cin>>str;
		if(str=="bye") break;
		recurse(0,0);
		cout<<ite<<". "<<num+1<<endl;
	}
}