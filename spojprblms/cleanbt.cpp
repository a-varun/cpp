#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[21][21];
int dp[21][21][1024];

int main(){
	ios::sync_with_stdio(false);
	int n,m;
	cin>>m>>n;
	while(n>0&&m>0){
		memset(ar,0,sizeof(ar));
		string s;
		memset(dp,-1,sizeof(dp));
		queue< pair<int, PII> > q;
		int count=1;
		REP(i,n){
			cin>>s;
			REP(j,m){
				if(s[j]=='*'){
					ar[i][j]=count++;
				}
				else if(s[j]=='x'){
					ar[i][j]=-1;
				}
				else if(s[j]=='o'){
					q.push(MP(i,MP(j,0)));
				}
			}
		}
		q.push(MP(-1,MP(-1,-1)));
		int flag=0, c=0;
		while(!q.empty()){
			int x=q.front().FST, y=q.front().SEC.first, mask = q.front().SEC.SEC;
			q.pop();
			if(x==y&&y==mask&&mask==-1){
				c+=1;
				if(q.empty()) break;
				q.push(MP(-1,MP(-1,-1)));
				continue;
			}
			if(x<0||x>=n||y<0||y>=m||ar[x][y]==-1||dp[x][y][mask]!=-1) continue;
			if(ar[x][y]>0){
				mask=mask|1<<(ar[x][y]-1);
			}
			if(mask==((1<<(count-1))-1)){
				flag=1;
				break;
			}
			dp[x][y][mask]=1;
			q.push(MP(x,MP(y+1,mask)));
			q.push(MP(x,MP(y-1,mask)));
			q.push(MP(x+1,MP(y,mask)));
			q.push(MP(x-1,MP(y,mask)));
		}
		if(flag)
			cout<<c<<endl;
		else
			cout<<"-1"<<endl;

		cin>>m>>n;	
	}
}