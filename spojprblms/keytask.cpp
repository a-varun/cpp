#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

struct node
{
	int x,y;
	int r,g,b,ye;
	int s;
	node(int x1, int y1, int R, int B, int G, int Y, int stp)
	{
		x=x1;
		y=y1;
		r=R;
		g=G;
		b=B;
		ye=Y;
		s=stp;
	}
};
int main()
{
	int r,c;
	scanf("%d%d",&r, &c);

	while(r&&c)
	{
		int flag=0,steps=0;
		vector<string> ar1;
		string  temp;
		node place(0,0,0,0,0,0,0);	
		REP(i,r)
		{
				cin>>temp;
			if(temp=="") {i--;continue;}
			ar1.PB(temp);
			REP(j,c)
			{

				if(ar1[i][j]=='*')
				{
							place.x=i;
							place.y=j;					
				}
			}
		}
		int te23=getchar();
		queue<node> ar;
		ar.push(place);
		int visited[r][c];
		memset(visited, 0, sizeof(visited));
		while(!ar.empty())
		{
			node p=ar.front();
			ar.pop();
			if(p.x<0||p.x>=r||p.y<0||p.y>=c)
				continue;
			if(ar1[p.x][p.y]=='#')
				continue;

			if(ar1[p.x][p.y]=='X'){
				flag=1;
				steps=p.s;
				break;
			}
			if(visited[p.x][p.y]&1)
			{
				if(visited[p.x][p.y]==p.r && visited[p.x][p.y][2]==p.b && visited[p.x][p.y][3]==p.g && visited[p.x][p.y][4]==p.ye )
					continue;
			}
			if(ar1[p.x][p.y]=='R'||ar1[p.x][p.y]=='B'||ar1[p.x][p.y]=='G'||ar1[p.x][p.y]=='Y')
			{
				if(ar1[p.x][p.y]=='R')
					if(p.r==0) continue;
				if(ar1[p.x][p.y]=='B')
					if(!p.b) continue;
				if(ar1[p.x][p.y]=='G')
					if(!p.g) continue;
				if(ar1[p.x][p.y]=='Y')
					if(!p.ye) continue;
			}
			if(ar1[p.x][p.y]=='r'||ar1[p.x][p.y]=='b'||ar1[p.x][p.y]=='g'||ar1[p.x][p.y]=='y')
			{
				if(ar1[p.x][p.y]=='r')
					p.r=1;
				if(ar1[p.x][p.y]=='b')
					p.b=1;
				if(ar1[p.x][p.y]=='g')
					p.g=1;
				if(ar1[p.x][p.y]=='y')
					p.ye=1;
			}
	
			visited[p.x][p.y][0]=1;
			if(visited[p.x][p.y][1]<p.r)visited[p.x][p.y][1]=p.r;
			if(visited[p.x][p.y][2]<p.b)visited[p.x][p.y][2]=p.b;
			if(visited[p.x][p.y][3]<p.g)visited[p.x][p.y][3]=p.g;
			if(visited[p.x][p.y][4]<p.ye)visited[p.x][p.y][4]=p.ye;
			node o(p.x+1,p.y,p.r,p.b,p.g,p.ye,p.s+1),tw(p.x-1,p.y,p.r,p.b,p.g,p.ye,p.s+1);
			node th(p.x,p.y+1,p.r,p.b,p.g,p.ye,p.s+1),fo(p.x,p.y-1,p.r,p.b,p.g,p.ye,p.s+1);
			ar.push(o);
			ar.push(tw);
			ar.push(th);
			ar.push(fo);
		}
		if(flag)
		{
			cout<<"Escape possible in "<<steps<<" steps."<<endl;
		}
		else
			cout<<"The poor student is trapped!"<<endl;
		scanf("%d%d",&r, &c);
	}
}


