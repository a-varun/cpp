#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

LL ar[100008];
int seive[100008];
int primes[1000006];
void prms(){
	primes[0]=primes[1]=1;
	for(int i=2;i<=1000;i++){
		if(seive[i]) continue;
		seive[i]=1;
		for(int j=i+i;j<=1000000;j+=i){seive[j]+=1;}
	}
}


int main(){
	int ite;
	cin>>ite;
	memset(primes,0,sizeof(primes));
	memset(ar,0,sizeof(ar));
	memset(seive,0,sizeof(seive));
	prms();
	int p[1000005][10]={0};
	
	for(int i=1;i<=1000000;i++){
		p[i][1]+=p[i-1][1];
		p[i][2]+=p[i-1][2];
		p[i][3]+=p[i-1][3];
		p[i][4]+=p[i-1][4];
		p[i][5]+=p[i-1][5];
		p[i][6]+=p[i-1][6];
		p[i][7]+=p[i-1][7];
		p[i][8]+=p[i-1][8];
		p[i][9]+=p[i-1][9];
		p[i][10]+=p[i-1][10];
		p[i][seive[i]]+=1;
	}
	while(ite--){
		int n,b,a;
		cin>>a>>b>>n;
		int count=0;
		cout<<p[b][n]-p[a-1][n]<<endl;
	}
}