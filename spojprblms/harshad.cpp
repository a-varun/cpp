#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define mx 1000000
#define smx 1000
int seive[mx+7];
inline int brkn(int n){int sum=0;while(n){sum+=n%10;n/=10;}return sum;}

int main(){
	ios::sync_with_stdio(false);
	int arr[mx+7]={0};
	seive[0]=1;
	seive[1]=1;
	for(int i=2;i<=smx;i++){
		if(seive[i]) continue;
		arr[i]=1;
		for(int j=i+i;j<=mx;j+=i) seive[j]=1;
	}
	for(int i=smx+1;i<=mx;i++) if(seive[i]==0) arr[i]=1;
	arr[0]=0;
	for(int i=1;i<=mx;i++){
		int s=brkn(i)+i;
		if(seive[s]==0) arr[s]=0;
	}
	for(int i=1;i<=mx;i++) arr[i]+=arr[i-1];
	int ite;
	cin>>ite;
	while(ite--){
		int a,b;
		cin>>a>>b;
		if(a==0) cout<<arr[b]<<endl;
		else
		cout<<(arr[b]-arr[a-1])<<endl;
	}
	return 0;
}





