#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		int l,m,n;
		cin>>l>>m>>n;
		int s[n];
		REP(i,n) cin>>s[i];
		string str;
		int tot=0;
		for(int i=0;i<m;i++){
			cin>>str;
			for(int j=0;j<str.length();j++){
				if(str[j]=='Y'){
					tot+=s[j]; s[j]=0;
				}
			}
		}
		tot=tot>l?l:tot;
		cout<<(tot)<<endl;
	}
}