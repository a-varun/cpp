#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int mp[1001][1001];
int vis[1001][1001];
int dir[4][2]={{0,1},{0,-1},{-1,0},{1,0}};
int main(){
	ios::sync_with_stdio(false);
	int ite;
	scanf("%d",&ite);
	while(ite--){
		deque<pair<int, pair<int, int> > > pq;
		int r,c;
		scanf("%d%d",&r,&c);
		getchar();
		for(int i=0;i<r;i++){
			for(int j=0;j<c;j++){
				mp[i][j]=getchar();
			}
			getchar();
		}
		int gc=INT_MAX;
		memset(vis,-1,sizeof(vis));

		pq.push_front(MP(0,MP(0,0)));
		while(!pq.empty()){
			int tc = pq.front().first, tx = pq.front().second.first, ty=pq.front().second.second;
			pq.pop_front();
			if(tx<0||tx>=r||ty<0||ty>=c) continue;
			if(tx==r-1&&ty==c-1) {gc=tc;break;}
			if(vis[tx][ty]!=-1 && vis[tx][ty]<=tc) continue;

			vis[tx][ty]=tc;
			REP(k,4){
				int ttx = tx+dir[k][0], tty=ty+dir[k][1];
				if(ttx<0||ttx>=r||tty<0||tty>=c) continue;
				if(mp[ttx][tty]==mp[tx][ty])
					pq.push_front(MP((tc+(!(mp[ttx][tty]==mp[tx][ty]))), MP(ttx, tty)));
				else
					pq.push_back(MP((tc+(!(mp[ttx][tty]==mp[tx][ty]))), MP(ttx, tty)));
			}

		}
		cout<<gc<<endl;
	}
}
