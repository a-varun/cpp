#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[404][404];
int visited[404], from[300];
int l,m,n,t;
inline int inp(){int n=0,s=1,c=getchar_unlocked();if(c=='-')s=-1;while(c<48)c=getchar_unlocked();while(c>47)n=(n<<3)+(n<<1)+c-'0',c=getchar_unlocked();return n*s;}
bool bfs()
{
    memset(visited, 0, sizeof(visited));
    queue <int> q;
    q.push(0);
    visited[0] = true;
    from[0] = -1;
    int brk=0;
    while (!q.empty())
    {
        int u = q.front();
        q.pop();
        for (int v=0; v<m+n+2; v++)
        {
            if (visited[v]==false && ar[u][v] > 0)
            {
                q.push(v);
                from[v] = u;
                visited[v] = true;
            	if(v==n+m+1){brk=1;break;}
            }
        }if(brk) break;
    }
    return (visited[n+m+1] == true);
}
int fordFulkerson()
{
    int u, v;
    int t=n+m+1,s=0;
    int max_flow = 0;  // There is no flow initially
    while (bfs())
    {
        int path_flow = INT_MAX;
        for (v=t; v!=s; v=from[v])
        {
            u = from[v];
            path_flow = min(path_flow, ar[u][v]);
        }
        for (v=t; v != s; v=from[v])
        {
            u = from[v];
            ar[u][v] -= path_flow;
            ar[v][u] += path_flow;
        }
        max_flow += path_flow;
    }
    return max_flow;
}

int main(){
	ios::sync_with_stdio(false);
	int ite;
	ite=inp();
	while(ite--){
		l=inp();m=inp();n=inp();
		memset(ar,0,sizeof(ar));
		REP(i,m) {ar[0][i+1]=inp();}
		REP(i,n) {ar[m+i+1][m+n+1]=inp();}
		REP(i,m){
			REP(j,n){
				ar[i+1][m+j+1] = inp();
			}
		}
		l=min(l,fordFulkerson());
		cout<<l<<endl;

	}
}