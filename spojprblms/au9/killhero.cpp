#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int h[51],a[51];
int n,hi,ai,t1,t2;
int dp[501][501];

int se[51];

int rec(){
	int dp[51][501][501];
	for(int i=0;i<=n;i++){
		for(int j=0;j<=ai;j++){
			for(int k=0;k<=hi;k++){
				if(i==0||j==0||k==0) dp[i][j][k]=0;
				else if(a[i-1]<j && h[i-1]<k){
					dp[i][j][k]=max(dp[i-1][j][k], 1+dp[i-1][j-a[i-1]][k-h[i-1]]);
				}
				else
					dp[i][j][k]=dp[i-1][j][k];

			}
		}
	}
	return dp[n][ai][hi];
}


int main(){
	ios::sync_with_stdio(false);
	int ite;
	cin>>ite;
	while(ite--){
		memset(se,0,sizeof(se));
		cin>>n>>hi>>ai;
		REP(i,n) cin>>h[i];
		REP(i,n) cin>>a[i];
		cout<<rec()<<endl;
	}
}
