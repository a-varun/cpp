#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef unsigned long long LL;
typedef pair<int, int> PII;
#define getchar_unlocked getchar
inline LL inp(){LL n=0,s=1,c=getchar_unlocked();if(c=='-')s=-1;while(c<48)c=getchar_unlocked();while(c>47)n=(n<<3)+(n<<1LL)+c-'0',c=getchar_unlocked();return n*s;}
long long n,k;
long long ar[1001];


LL cnt(LL maxval){
	LL totcnt=0, totcnt1=0;

	for(int i=0;i<n;i++){
		// for(LL j=1;j<=k;j++){
		// 	if((j*ar[i] + (j*(j-1LL))/2LL)<=maxval)
		// 		totcnt++;
		// 	else break;
			
		// }
		LL lo=1, hi=k;
		while(hi>=lo){
		    LL mid = (lo + hi)/2LL; 
		   	LL mcnt = (mid*ar[i] + (mid*(mid-1LL))/2LL);

		    if((mid == k || maxval < ((mid+1)*ar[i] + ((mid+1)*((mid+1)-1LL))/2LL)) && mcnt <= maxval)
		      {lo=mid; break;}
		    else if(maxval <= mcnt)
		      hi=mid-1;
		    else
		      lo=mid+1;
		  }
		  totcnt1+=lo;
		}
	// if(totcnt1!=totcnt) cout<<maxval<<' '<<totcnt<<' '<<totcnt1<<endl;
	// if(totcnt1!=totcnt) exit(-1);
	return totcnt1;
}

int main(){
	ios::sync_with_stdio(false);
	int ite;
	cin>>ite;
	while(ite--){
		cin>>n>>k;
		long long ans=0, t1;
		long long eggs=0;
		long long t2;
		for(long long i=0;i<n;i++){
			cin>>ar[i];
		}
		LL lo=1, hi = 15000000000000000LL;
		while(hi >= lo)
		{
		    LL mid = (lo + hi)/2LL; 
		   	LL mcnt = cnt(mid);
		    if((mid == 0 || k > cnt(mid-1)) && mcnt >= k)
		      {lo=mid; break;}
		    else if(k > mcnt)
		      lo=mid+1;
		    else
		      hi=mid-1;
		}
		  cout<<lo<<endl;
	}
}