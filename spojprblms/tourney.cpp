#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>

#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>

using namespace std;

#define REP(i, n) for(long int i=0; i<(n); i++)
#define REPAB(i, a, b) for(long int i=(a); i<(b); i++)
#define NREPAB(i, a, b) for(long int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(long int i=(a); i<(b); i+=(c))
#define SPOJ long int ite;scanf("%d",&ite); while(ite--)

#define SS(x) scanf("%d", &x);
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) a.sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<long int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<long int, long int> PII;

#define SL(a) a.length()
#define VL(x) ((long int)x.size())
#define RE(x) return x;

struct node
{
  long int mp, mv, lvl;
};

long flag[2097152];
long int a[1048577];
node tree[2097152]; 

void build(long int lo, long int hi, long int i)
{
  if(lo==hi)
  {
    tree[i].lvl=0;tree[i]=ar[lo]
    tree[i].mp=lo;
    tree[i].mv=a[lo];
    return;
  }
  long int mid=(hi+lo)/2;
  build(lo,mid,2*i+1);
  build(mid+1,hi,2*i+2);
  tree[i].lvl=tree[2*i+1].lvl+1;
  if(tree[2*i+1].mv>tree[2*i+2].mv)
  {
    tree[i].mp=tree[2*i+1].mp;
    tree[i].mv=tree[2*i+1].mv;
    tree[i].lvl=tree[2*i+1].lvl+1;
  }
  else 
  {
    tree[i].mp=tree[2*i+2].mp;
    tree[i].mv=tree[2*i+2].mv;
    tree[i].lvl=tree[2*i+2].lvl+1;
  }
  return;
}

void update(long int lo,long int hi, long int pos, long int val, long int i)
{
  if(lo>pos||hi<pos)
    return;
  long int mid=(hi+lo)/2;
  if(lo==hi)
  {
    tree[i].mv=val;
    return;
  }
  if(flag[i]==1)
  {
    update(lo,mid,tree[i].mp,tree[i].mv,2*i+1);
    update(mid+1,hi,tree[i].mp,tree[i].mv,2*i+2);
    flag[i]=0;
  }
  if(tree[i].mv<val)
  {
    tree[i].mv=val;
    tree[i].mp=pos;
    flag[i]=1;
    return;
  }
  
  update(lo,mid,pos,val,2*i+1);
  update(mid+1,hi,pos,val,2*i+2);
  if(tree[2*i+1].mv>tree[2*i+2].mv)
  {

    tree[i].mp=tree[2*i+1].mp;
    tree[i].mv=tree[2*i+1].mv;

  }
  else 
  {
    tree[i].mp=tree[2*i+2].mp;
    tree[i].mv=tree[2*i+2].mv;
  }

}

long int query(long int lo, long int hi, long int pos, long int i)
{
  if(lo>pos||hi<pos)
    return 0;
  long int mid=(hi+lo)/2;
  if(flag[i]==1)
  {
    update(lo,mid,tree[i].mp,tree[i].mv,2*i+1);
    update(mid+1,hi,tree[i].mp,tree[i].mv,2*i+2);
    flag[i]=0;
  }
  if(tree[i].mp==pos)
  {
    return tree[i].lvl;
  }
  else
  {
    return query(lo,mid,pos,2*i+1)+query(mid+1,hi,pos,2*i+2);
  }
}

char ch;
long int rt,yu;
int main()
{
  long int n,q;
  scanf("%ld%ld",&n,&q);
  n=pow(2,n);
  for(long int i=0;i<n;i++)
  {
    scanf("%ld",&a[i]);
  }
  build(0,n-1,0);
  while(q--)
  {
   /* for(int i=0;i<2*n-1;i++)
    {
      cout<<"Level:"<<i<<'\t'<<"Pos:"<<tree[i].mp<<'\t'<<"Val:"<<tree[i].mv<<'\t'<<"Flip:"<<flag[i]<<'\t'<<endl;
    }*/
    cin>>ch;
    if(ch=='W')
    {
      cout<<tree[0].mp+1<<endl;
    }
    else if(ch=='S')
    {
      cin>>rt;
      cout<<query(0,n-1,rt-1,0)<<endl;
    }
    else
    {
      cin>>rt>>yu;
      update(0,n-1,rt-1,yu,0);
    }
  }
}

