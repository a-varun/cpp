#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int tree[3000000][4];
int k;
int ar[1000005];
#define lmax [0]
#define updval [3]
#define rmax [1]
#define dirty [2]

void build(int lo, int hi, int i){
	if(lo==hi){
		tree[i]lmax = tree[i]rmax= k;
		tree[i]dirty = 0;
		return;
	}
	int mid=(hi+lo)/2;
	build(lo,mid,2*i+1);build(mid+1,hi,2*i+2);
	tree[i]lmax = max(tree[2*i+1]lmax,tree[2*i+2]rmax);
	tree[i]rmax = max(tree[2*i+1]rmax,tree[2*i+2]rmax);
	tree[i]dirty = 0;
}

int update(int lo, int hi, int val, int i){
	int mid=(lo+hi)/2;
	if(tree[i]dirty){
		if(tree[i]dirty ==1)
			update(lo,mid,tree[i]updval,2*i+1);
		else
			update(mid+1,hi,tree[i]updval,2*i+2);
		tree[i]lmax = max(tree[2*i+1]lmax,tree[2*i+1]rmax);
		tree[i]rmax = max(tree[2*i+2]rmax,tree[2*i+2]rmax);
		tree[i]dirty=0;
		tree[i]updval=0;
	}
	if(tree[i]lmax<val && tree[i]rmax<val) return -1;
	if(lo==hi){
		tree[i]lmax-=val;
		tree[i]rmax-=val;
		return 1;
	}
	if(tree[i]lmax>=val)
		{tree[i]lmax-=val;tree[i]dirty=1;}
	else if(tree[i]rmax>=val)
		{tree[i]rmax-=val;tree[i]dirty=2;}
	tree[i]updval=val;
	return 1;
}

int coun=0;
void teaverse(int lo, int hi, int i){
	int mid=(lo+hi)/2;
	if(lo==hi){
		coun+= tree[i]lmax != k;
		return;
	}
	if(tree[i]dirty){
		if(tree[i]dirty==1)
			update(lo,mid,tree[i]updval,2*i+1);
		else
			update(mid+1,hi,tree[i]updval,2*i+2);
		tree[i]dirty=0;
		tree[i]updval=0;
	}
	teaverse(lo,mid,2*i+1);teaverse(mid+1,hi,2*i+2);
}

void print(int lo, int hi, int i){
	if(lo==hi){
		cout<<i<<' '<<tree[i]lmax<<' '<<tree[i]rmax<<' '<<tree[i]dirty<<' '<<tree[i]updval<< endl;
		return;
	}
		cout<<i<<' '<<tree[i]lmax<<' '<<tree[i]rmax<<' '<<tree[i]dirty<<' '<<tree[i]updval<< endl;
	int mid=(lo+hi)/2;
	print(lo,mid,2*i+1);print(mid+1,hi,2*i+2);	
}

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		string s;
		int n;
		coun=0;
		cin>>k>>n;
		memset(tree,0,sizeof(tree));
		build(0,n-1,0);
		int c=0,a,b,st=0;
		while(c<n){
			cin>>s;
		// print(0,n-1,0);
		// cout<<endl;
			if(s=="b"){
				cin>>a>>b;
				REP(s,a) update(0,n-1,b,0);
				c+=a;
				st+= a*b;
				continue;
			}
			else{
				stringstream kk;
				kk<<s;
				kk>>a;
				update(0,n-1,a,0);
			}
			st+=a;
			c++;
			continue;
		}
		// print(0,n-1,0);
		// cout<<endl<<endl;
		teaverse(0,n-1,0);
		// print(0,n-1,0);
		cout<<coun<<' ';
		cout<< (coun*k - st) <<endl;

	}

}