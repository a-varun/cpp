#include<iostream>              //------------------------------------------------------------//
#include<cstdio>                //  ___  ___ _____ ______                                     //
#include<algorithm>             //  |  \/  |/  ___|| ___ \     This C++ Template Belongs to   //                   
#include<cmath>                 //  | .  . |\ `--. | |_/ /        Manish Singh Bisht          //
#include<vector>                //  | |\/| | `--. \| ___ \       http://fb.me/manish05        //    
#include<set>                   //  | |  | |/\__/ /| |_/ /    Email: manish05@facebook.com    //
#include<map>                   //  \_|  |_/\____/ \____/                                     //    
#include<functional>            //------------------------------------------------------------//
#include<string>
#include<cstring>
#include<bitset>
#include<cstdlib>
#include<queue>
#include<utility>
#include<fstream>
#include<sstream>

#include<stack>
#include<cstdio>
#include<ctime>
         
using namespace std;
#define gc getchar_unlocked
#define MEM(a,b) memset(a,(b),sizeof(a))
#define FOR(i,n) for(int i=(0);i<(n);i++)
#define CLEAR(a) memset((a),0,sizeof(a))
#define S(n) scanf("%d", &n)
#define P(k) printf("%d\n", k)
#define fastS(n) scanint(&n)
#define pb push_back
#define mp make_pair
#define ll long long
#define VI vector<int>
#define PII pair<int, int>
#define ft first
#define sd second
#define inf (1<<30)
#define PNL printf("\n")
#define md 1000000007
#define Swap(a,b) {*b = (*a + *b) - (*a = *b);}
#define swap(a,b) Swap(&a,&b)
#define bigger(a,b) {a>b?a:b}
#define smaller(a,b) {a<b?a:b}
#define fastS(n) scanint(&n)
void scanint(int *x)
{
    register int c = gc();
    *x = 0;
    for(;(c<48 || c>57);c = gc());
    for(;c>47 && c<58;c = gc()) {*x = (*x<<1) + (*x<<3) + c - 48;  }
}

ll modPow(ll a,ll b)
{ll x;
    for(x=1;b>0;b/=2,a=(a*a)%md)
        if(b%2==1)
                x=(x*a)%md;
  return x%md;
}
            
string convertInt(int number){stringstream ss;ss << number;return ss.str();}
int convertString(string s){int num;stringstream sstr(s);sstr>>num;return num;}

int precomp[100001];
vector<VI> divisors;
int n,q,ar[100001],l,r,k;
int main()
{
    CLEAR(precomp);
    divisors.resize(100001);
    fastS(n);fastS(q);
    FOR(i,n){
    
    fastS(ar[i]);
    
    for(int j=1;j*j<=ar[i];j++){
        if(ar[i]%j==0){
            divisors[j].pb(i+1);
            if(ar[i]!=j*j){
                divisors[ar[i]/j].pb(i+1);
            }
        }
    }
    
    }
    while(q--){
        fastS(l);
        fastS(r);
        fastS(k);
        if(precomp[k]==0){
            sort(divisors[k].begin(),divisors[k].end());
            precomp[k]=1;
        }
        
        P(upper_bound(divisors[k].begin(),divisors[k].end(),r)-lower_bound(divisors[k].begin(),divisors[k].end(),l));
    }
    return 0;
}