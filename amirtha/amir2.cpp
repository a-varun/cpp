		#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
vector<string> ar;
int check(int rs1,int cs1,int rl1,int cl1)
{
	int r=ar.size(),flag;
	int c=ar[0].length();
	int rl=(rl1-rs1+1);
	int cl=(cl1-cs1+1);
	for(int i=0;i<r;i++)
	{	
		if(i+rl-1>=r) break;
		for(int j=0;j<c;j++)
		{
			if(j+cl-1>=c) break;
			if(i==rs1&&j==cs1) continue;
			flag=0;
			for(int k=0;k<rl;k++)
			{
				for(int l=0;l<cl;l++)
				{
					if(ar[k+i][l+j]==ar[k+rs1][l+cs1]) continue;
					flag=1;break;
				}
				if(flag) break;
			}
			if(!flag){/*printf("%d %d   %d %d", rs1, cs1,i,j );*/ return 1;}
		}
	}
	return 0;
}

int main()
{
	TCases
	{
		ar.clear();
		string temp;
		int r,c;
		scanf("%d%d",&r,&c);
		REP(i,r)
		{
			cin>>temp;
			ar.PB(temp);
		}
		int imax=0;
		for(int i=0;i<r;i++)
		{
			for(int j=0;j<c;j++)
			{
				int sum=0;
				for(int k=i;k<r;k++)
				{
					for(int l=j;l<c;l++)
					{
						sum=check(i,j,k,l);
						if(sum>0)
						{
							imax=max(imax, (k-i+1)*(l-j+1));
						}
					}
				}
			}
		}
		printf("%d\n",imax);
	}
}