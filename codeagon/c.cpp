#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
 
int tree[100009][2], ar[100009];

int counti(int lo, int hi,int x, int y, int i, int maxi){
	if(lo>x || hi<y) return 0;
	if(lo==hi){
		if(tree[i][0]==maxi){
			return 1;
		}
		else
			return 0;
	}
	if(tree[i][0]==maxi && lo>=x && hi<=y ){
		return tree[i][1];
	}
	int mid = (lo+hi)/2;
	return counti(lo,mid,x,y,2*i+1,maxi) + counti(mid+1,hi,x,y,2*i+2,maxi);
}
void build(int lo, int hi, int i){
	if(lo==hi){
		tree[i][0]=ar[lo];
		tree[i][1]=1;
		return;
	}
	int mid = (lo+hi)/2;
	build(lo, mid, 2*i+1);build(mid+1, hi, 2*i+2);
	if(tree[2*i+1][0]>tree[2*i+2][0]){
		tree[i][0] = tree[2*i+1][0];
		tree[i][1] = tree[2*i+1][1] + counti(mid+1, hi,mid+1, hi, 2*i+2, tree[i][0]);
	}
	else{
					tree[i][0] = tree[2*i+2][0];
		tree[i][1] = tree[2*i+2][1] + counti(lo, mid,lo, mid, 2*i+1, tree[i][0]);
	}
}



PII query(int lo, int hi, int x, int y, int i){

	if(lo>y || hi<x){
		return MP(-1,-1);
	}
	if(lo==hi){
		return MP(tree[i][0], tree[i][1]);
	}
	if(lo>=x && hi<=y){
		return MP(tree[i][0],tree[i][1]);
	}
	int mid = (lo+hi)/2;

	PII l = query(lo,mid,x,y,2*i+1), r = query(mid+1,hi,x,y,2*i+2);
	if(l.first == -1 && r.first == -1) return l;
	if(l.first == -1) return r;
	if(r.first == -1) return l;
	if(l.first > r.first){
		return MP(l.first, l.second + counti(mid+1, hi,x,y,2*i+2 , l.first));
	}
	else{
		return MP(r.first, r.second + counti(lo,mid,x,y, 2*i+1,r.first));
	}
    return MP(-1,-1);
}

int main(){
	int n,m;
	cin>>n>>m;
	REP(i,n) cin>>ar[i];
	build(0,n-1,0);
	while(m--){
		int t1,t2;
		cin>>t1>>t2;
        t1--;t2--;
		cout<<query(0,n-1,t1,t2,0).second<<endl;
	}	
}



