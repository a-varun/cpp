import sys,random,string;

def reve(a):
	return a[::-1]

filen = sys.argv[1]
n = 100
f = open(filen, "w")
fout = open(filen+".out", "w")
f.write(str(n)+"\n")
while n>0:
	n-=1
	words = random.randint(10,15)
	stri = ""
	for i in range(words):
		k = random.randint(5,10)
		stri += ''.join(random.choice(string.ascii_lowercase) for _ in range(k))
		stri += " "
	stri = stri[:-1]	
	print stri
	f.write(stri+"\n")
	fout.write(  " ".join(map(reve, stri.split(' ')))+'\n')
f.close()
fout.close()