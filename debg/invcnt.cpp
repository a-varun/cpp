#include <stdio.h>
#include <stdlib.h>
int do_other_stuff (int *A, int a, int *B, int b, int *C) 
{
  int i,j,k;
  int r;
  while (i < a && j < b) {
    if (A[i] <= B[j]) {
	  C[k] = A[i];
	  i++;
	  k++;
    r = r + (a - i);  
    }

    C[k] = B[j];
    j++;
    k++;
  }
  while (i < a) {
    C[k]= A[i];
    i++;
    k++;
  }
  while (j < b)  {
    C[k]= B[j];
    j++;
    k++;
  }
  return r;
}  
int do_stuff(int *A, int n) 
{
  int i;
  int *A1, *A2;
  int n1, n2;
  int r1, r2, r;
  n1 = n / 2;
  n2 = n - n1;
  A1 = (int*)malloc(sizeof(int) * n1);
  A2 = (int*)malloc(sizeof(int) * n2);
  for (i =0; i < n1; i++) {
    A1[i] = A[i+n1];
  }
  for (i = 0; i < n2; i++) {
    A2[i] = A[i+n2];
  }
  r1 = do_stuff(A, n1);
  r2 = do_stuff(A2, n2);
  r = do_other_stuff(A1, n1, A2, n2, A);
  free(A1);
  free(A2);
  return r1+r2;
}
int main(int argv, char** args)
{
 int ite;
 scanf("%d",&ite);
 while(ite--){
  int i, n;
  int *A;
  int r;
  char c;
  scanf("%d", &n);
  A = (int *)malloc(sizeof(int) * n);
  for (i = 0; i < n; i++) {
    scanf("%d", &(A[i]));
  }
  r = do_stuff(A, n);
  printf("%d\n", r);
  free(A);  
} 
}