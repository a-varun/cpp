/**
 *  sortcount.c 
 *  Sort the inputted integers and count the inversions
 **/


#include <stdio.h>
#include <stdlib.h>
int merge_count (int *A, int a, int *B, int b, int *C) 
{
  int i,j,k;
  int r;
  i = 0; 
  j = 0;
  k = 0;
  r = 0;
  while (i < a && j < b) {
    if (A[i] <= B[j]) {
	  C[k] = A[i];
	  i++;
	  k++;
    }
    else {
      C[k] = B[j];
      r = r + (a - i);  
      j++;
      k++;
    }
  }
  while (i < a) {
    C[k]= A[i];
    i++;
    k++;
  }
  while (j < b)  {
    C[k]= B[j];
    j++;
    k++;
  }
  return r;
}  
int sort_count(int *A, int n) 
{
  int i;
  int *A1, *A2;
  int n1, n2;

  int r1, r2, r;

  if (n < 2)
    return 0;
  n1 = n / 2;
  n2 = n - n1;
  A1 = (int*)malloc(sizeof(int) * n1);
  A2 = (int*)malloc(sizeof(int) * n2);
  for (i =0; i < n1; i++) {
    A1[i] = A[i];
  }
  for (i = 0; i < n2; i++) {
    A2[i] = A[i+n1];
  }
  r1 = sort_count(A1, n1);
  r2 = sort_count(A2, n2);
  r = merge_count(A1, n1, A2, n2, A);
  free(A1);
  free(A2);
  return r1+r2+r;
}
int main(int argv, char** args)
{
 int ite;
 scanf("%d",&ite);
 while(ite--){
  int i, n;
  int *A;
  int r;
  char c;
  scanf("%d", &n);
  A = (int *)malloc(sizeof(int) * n);
  for (i = 0; i < n; i++) {
    scanf("%d", &(A[i]));
  }
  r = sort_count(A, n);
  printf("%d\n", r);
  free(A);
  
} 
}