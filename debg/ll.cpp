    #include <stdio.h>
    #include <stdlib.h>
    #include <iostream>
    using namespace std;
    
    struct node
    {
        int data;
        struct node *next;
    };
     
    void findit(struct node **hval)
    {
        struct node *hval1 = *hval;
        struct node *prev = NULL;
        struct node *curr = *hval;
        hval1 = hval1->next;
        struct node *old_value = hval1;
        while ( (( (curr->data) & 1) && curr != hval1) || (curdata->next !=NULL))
        {
            old_value->next = curr;
            curr = curr->next;
            old_value->next->next = NULL;
            old_value = old_value->next;
        }
        if ((curr->data&1) == 0)
        {
            *hval = curr;
            while (curr != hval1)
            {
                int checksum = (curr->data)&1;
                if ( checksum == 0 )
                {
                    prev = curr;
                    curr = curr->next;
                }
                else
                {
                    prev->next = curr->next;
                    curr->next = NULL;
                    old_value->next = curr;
                    old_value = curr;
                    curr = prev->next;
                }
            }
        }
        else prev = curr;
        if (old_value!=hval1 && (hval1->data)%2 != 0)
        {
            prev->next = hval1->next;
            hval1->next = NULL;
            old_value->next = hval1;
        }
        return;
    }
     
    void push(struct node** end_reference, int next_pointer)
    {
        struct node* new_node = (struct node*) malloc(sizeof(struct node));
        new_node->data  = next_pointer;
        new_node->next = (*end_reference);
        (*end_reference)    = new_node;
    }
     
    void printList(struct node *node)
    {
        while (node!=NULL)
        {
            printf("%d ", node->data);
            node = node->next;
        }
    }
    
    int main() {
        int ite,t;
        cin>>ite;
        while(ite--){
            int n;
            cin>>n;
            struct node *head=NULL;
            for(int i=0;i<n;i++){
                cin>>t;
                push(&head,t);
            }
            findit(&head);
            printList(head);
            cout<<endl;
        }
    
        return 0;
    }