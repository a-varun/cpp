#include <map>
#include <cmath>
#include <set>
#include <cstring>
#include <stack>
#include <vector>
#include <queue>
#include <list>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <climits>
using namespace std;
#define si(x) scanf("%d" , &x)
#define sl(x) scanf("%ld" , &x)
#define sll(x) scanf("%lld" , &x)
#define sc(x) scanf("%c" , &x)
#define ss(x) scanf("%s" , &x)
#define pi(x) printf("%dn" , x)
#define pl(x) printf("%ldn" , x)
#define pll(x) printf("%lldn" , x)
#define pc(x) printf("%cn" , x)
#define ps(x) printf("%sn" , x)
#define fup(i,a,b) for(int i=a;i<b;i++)
#define fdn(i,a,b) for(int i=a;i>=b;i--)
#define DEBUG 0
#define gc getchar
#define ll long long
#define SEPERATOR " : "
#define trace1(a) cout << #a << SEPERATOR << a << endl;
#define trace2(a,b) cout << #a << SEPERATOR << a << SEPERATOR << #b << SEPERATOR << b << endl;
#define trace3(a,b,c) cout << #a << SEPERATOR << a << SEPERATOR << #b << SEPERATOR << b << #c << SEPERATOR << c << endl;
void scanint(ll int &x) {
    register ll int c = gc();
    x = 0;
    for (; (c < 48 || c > 57); c = gc())
    ;
    for (; c > 47 && c < 58; c = gc()) {
        x = (x << 1) + (x << 3) + c - 48;
    }
}
int readline(char * str) {
    int i = 0;
    char ch;
    while ((ch = getchar()) != 'n') {
        str[i++] = ch;
    }
    str[i] = '\0';
    return i;
}
#define ASIZE 10
int arr[ASIZE][ASIZE];
int visited[ASIZE][ASIZE];
int row = 0, col = 0;
int neighborx[4] = { -1, 1, 0, 0};
int neighbory[4] = { 0, 0, -1, 1};
int is_valid(int x, int y) {
    if (x < row && y < col && x >= 0 && y >= 0 && arr[x][y] >=1) {
        return 1;
    }
    return 0;
}
int dfs(int x, int y) {
    int count = arr[x][y];
    for(int i=-1;i<=1;i++){
        for(int j=-1;j<=1;j++){
            if((!i&&!j)||(i&&j)) continue;
        int newx = x + neighborx[i];
        int newy = y + neighbory[i];
        if (is_valid(newx, newy) && visited[newx][newy] == 0) {
            visited[newx][newy] = 1;
            count+=dfs(newx, newy);
        }
    }
    return count;
}
int islands() {
    memset(visited, 0, sizeof(visited));
    int count = 0;
    int maxi=-1;
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            if (visited[i][j] == 0 && arr[i][j] > 0) {
                visited[i][j] = 1;
                maxi = max(maxi,dfs(i, j));
                count++;
            }
        }
    }
    return maxi;
}
int main(int argc, char * argv[]) {
    int ite;
    cin>>ite;
    while(ite--){
    cin>>row;
    col=row;
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            cin>>arr[i][j];
        }
    }
    printf("%d\n", islands());
}}