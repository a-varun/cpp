#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[100][100];
int n,m;
int rec(int x,int y){
	if(x<0||x>=n||y<0||y>=m) return 0;
	int maxi=0;
	for(int i=-1;i<=1;i++){
		for(int j=-1;j<=1;j++){
			if(i==0&&j==0) continue;
			if(i==0||j==0){
				int tx=x+i, ty=y+j;
				if(tx<0||tx>=n||ty<0||ty>=m) continue;
				if(ar[tx][ty]==ar[x][y]+1){
					maxi=max(maxi, rec(tx,ty));
				}
			}
		}
	}
	return maxi+1;
}

int main(){
	cin>>n>>m;
	
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++) cin>>ar[i][j];
	}
	int maxi=0;
	for(int i=0;i<n;i++){
		REP(j,m){
			maxi=max(maxi,rec(i,j));
		}
	}
	cout<<maxi<<endl;
}