#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main(){

	int ite;
	scanf("%d",&ite);
	while(ite--){
		long long int n,m=0,s=0;
		scanf("%lld",&n);
		long long int ar[n];
		for(int i=0;i<n;i++)scanf("%d",&ar[i]);
		for(int i=0;i<n;i++){
			s+=ar[i];
			m+=abs(s);
		}
		printf("%lld\n",m);
}}