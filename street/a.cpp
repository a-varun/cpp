#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int n;
	int ite;
	cin>>ite;
	while(ite--){
	cin>>n;
	int t;
	vector<int> vtr;
	for(int i=0;i<n;i++){
		cin>>t;
		vtr.PB(t);
	}
	vector<int> vtr1,vtr2;
	REP(i,n) vtr1.PB(vtr[i]);
	REP(i,n) vtr2.PB(vtr[i]);
	SORT(vtr1);
	if(!next_permutation(vtr2.begin(),vtr2.end())){
		REP(i,n) cout<<vtr[i]<<endl;
		return 0;
	}
	vtr2=vtr1;
	while(1){
		next_permutation(vtr1.begin(),vtr1.end());
		int fl=1;
		REP(i,n) if(vtr1[i]!=vtr[i]) fl=0;
		if(fl==1){
					REP(i,n) cout<<vtr2[i]<<endl;

			return 0;
		} 
		vtr2.clear();
		REP(i,n) vtr2.PB(vtr1[i]);
		}}
}