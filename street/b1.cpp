#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int n,t;
	cin>>n;
	vector<int> vtr;
	REP(i,n){
		cin>>t;
		vtr.PB(t);
	}
	int dp[n];
	memset(dp,0,sizeof(dp));
	dp[0]=1;
	for(int i=1;i<n;i++){
		int mini=1;
		for(int j=0;j<i;j++){
			if(vtr[j]<vtr[i]) mini=max(mini,dp[j]+1);
		}
		dp[i]=mini;
	}
	int maxi=-1;
	REP(i,n) maxi=max(maxi,dp[i]);
	if((n==3)||n==6 || (n>5 && n%2==0))
		cout<<"Sharukh"<<endl;
	else
		cout<<"Amar"<<endl;
}