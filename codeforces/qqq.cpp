#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define rep(i, n) for(int i=0; i<(n); i++)
#define si(x) ((int)x.size())
#define pb(x) push_back(x)
#define sort(a) sort(a.begin(),a.end())
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main()
{
	int n,m,k;
	scanf("%d%d%d",&n,&m,&k);
	string temp;
	int ar[n][m];
	memset(ar,0,sizeof(ar));
	REP(i,n)
	{
		cin>>temp;
		REP(j,m)
		{
			if(temp[j]=='#')
			{
				ar[i][j]=-1;
			}
			else if(temp[j]=='.')
			{
				ar[i][j]=0;
			}
		}
	}
	REP(i,n)
	{
		REP(j,m)
		{
			if(ar[i][j]==-1) continue;
			int cnt=0;
			if(i>0)
				cnt+=ar[i-1][j]>=0;
			if(j>0)
				cnt+=ar[i][j-1]>=0;
			if(i<n-1)
				cnt+=ar[i+1][j]>=0;
			if(j<m-1)
				cnt+=ar[i][j+1]>=0;
			ar[i][j]=cnt;
		}
	
	}


	int count=0;
	while(count<k)
	{
		
		REP(i,n){
		REP(j,m)
		{
		if(ar[i][j]==1)
		{
			

			count++;
			ar[i][j]=-2;
			if(i>0)
				ar[i-1][j]= ar[i-1][j]<=0 ? ar[i-1][j] : ar[i-1][j]-1;
			if(j>0)
				ar[i][j-1]=ar[i][j-1]<=0?ar[i][j-1]:ar[i][j-1]-1;
			if(i<n-1)
				ar[i+1][j]=ar[i+1][j]<=0?ar[i+1][j]:ar[i+1][j]-1;
			if(j<m-1)
				ar[i][j+1]=ar[i][j+1]<=0?ar[i][j+1]:ar[i][j+1]-1;
cout<<endl;
			if(count==k) break;
		}}if(count==k) break;}
	}


	REP(i,n){
	REP(j,m)
	{
		if(ar[i][j]==-1) cout<<'#';
		if(ar[i][j]==-2) cout<<'X';
		if(ar[i][j]>=0) cout<<'.';
	}
	cout<<endl;
}
	}

