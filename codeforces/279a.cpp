#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	
	int ar[3]={0};
	int n;
	cin>>n;
	int stud[n];
	int t;
	for(int i=0;i<n;i++){
		cin>>t;
		stud[i]=t;
		ar[t-1]++;
	}
	int c=(min(ar[0],min(ar[1],ar[2])));
	cout<<c<<endl;
	for(int k=0;k<c;k++){
		for(int i=0;i<n;i++)
			if(stud[i]==1){cout<<i+1<<' ';stud[i]=-1;break;}
		for(int i=0;i<n;i++)
			if(stud[i]==2){cout<<i+1<<' ';stud[i]=-1;break;}
		for(int i=0;i<n;i++)
			if(stud[i]==3){cout<<i+1<<endl;stud[i]=-1;break;}
	}
}