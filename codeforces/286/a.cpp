#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[30004];
int maxi;
int dp[30001][30001];

long long rec(int pstart, int dist){
	if(curr>maxi) return 0;
	if(dist<=0) return 0;
	if(dp[pstart][dist]!=-1) return dp[pstart][dist];
	int curr = pstart+dist;
	int maxg = max(rec(curr,dist-1),max(rec(curr,dist+1),rec(curr,dist)));
	return dp[pstart][dist]= maxg + ar[curr];
}

int main(){
	int n,t,d;
	memset(dp,-1,sizeof(dp));
	memset(ar,0,sizeof(ar));
	maxi=-1;
	cin>>n>>d;
	for(int i=0;i<n;i++){
		cin>>t;
		maxi=max(maxi,t);
		ar[t]+=1;
	}
	long long tot = rec(0,d);
	cout<<tot<<endl;
}