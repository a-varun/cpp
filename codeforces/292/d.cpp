#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define TRACE

#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
#define getchar_unlocked getchar
#define putchar_unlocked putchar

#else

#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
#endif
int n,m;
VS vtr;
int vals[100][100];
int ans[100][100];
int dx[]={-1,1,0,0};
int dy[]={0,0,-1,1};

bool do_func(){
	int p=0;
	// REP(i,n){
	// 		REP(j,m) cout<<(char)ans[i][j];
	// 	cout<<endl;}
	// 	cout<<endl;
	// 	getchar();
	REP(i,n)REP(j,m){
		if(vtr[i][j]=='.') p++;
	}
	if(!p){ return true;}
	REP(i,n){
		REP(j,m){
		if(vtr[i][j]!='.'){continue;}
			int c=0;
			REP(k,4){
				int tx=i+dx[k],ty=j+dy[k];
				if(tx<0||tx>=n||ty<0||ty>=m||vtr[tx][ty]!='.') continue;
				c++;
			}
	
		if(c==1)
			REP(k,4){
				int tx=i+dx[k],ty=j+dy[k];
				if(tx<0||tx>=n||ty<0||ty>=m||vtr[tx][ty]!='.') continue;			
				
				if(dx[k]==1){
					vtr[i][j]='^';
					vtr[tx][ty]='v';
				}
				else if(dx[k]==-1){
					vtr[i][j]='v';
					vtr[tx][ty]='^';
				}
				else if(dy[k]==-1){
					vtr[i][j]='>';
					vtr[tx][ty]='<';
				}
				else{
					vtr[i][j]='<';
					vtr[tx][ty]='>';
				}
				if(do_func()) return true;
				vtr[tx][ty]=vtr[i][j]='.';
				}
			}	
		}
		return false;
		return true;
	}

int main(){
	memset(ans,-1,sizeof(ans));
	cin>>n>>m;
	REP(i,n){
		S t;
		cin>>t;
		vtr.PB(t);
	}
	string inv="Not unique";
	REP(i,n){
		REP(j,m){
			if(vtr[i][j]=='*'){ vals[i][j]=-1;ans[i][j]='*';  continue;}
			int c=0;
			REP(k,4){
				int tx=i+dx[k],ty=j+dy[k];
				if(tx<0||tx>=n||ty<0||ty>=m||vtr[tx][ty]=='*') continue;
				c++;
			}
			vals[i][j]=c;
		}
	}
	//REP(i,n){REP(j,m)cout<<vals[i][j]<<' ';cout<<endl;}
	int fl=0;
	REP(i,n)REP(j,m){
		if(vals[i][j]==0) fl=1;
	}
	if(fl){
		cout<<inv<<endl;return 0;
	}
	if(do_func()){
		REP(i,n){
			cout<<vtr[i]<<endl;}
	}else
	cout<<inv<<endl;
}