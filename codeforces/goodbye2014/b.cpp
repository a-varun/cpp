#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
vector<string> s;
int ar[500],n;

void bfs(){

	int mini=ar[0];
	for(int i=0;i<n;i++){
		for(int j=i+1;j<n;j++){
			if(ar[i]>ar[j]&&s[i][j]=='1'){
				swap(ar[i],ar[j]);
			}
		}
	}
}

int main(){
	cin>>n;
	for(int i=0;i<n;i++) cin>>ar[i];
	string s1;
	for(int i=0;i<n;i++){
		cin>>s1;
		s.PB(s1);
	}
	for(int k=0;k<n;k++){
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(s[i][k]=='1'&&s[k][j]=='1'){
					s[i][j]='1';
				}
			}
		}
	}
	bfs();
	for(int i=0;i<n;i++) cout<<ar[i]<<' ';cout<<endl;
}







