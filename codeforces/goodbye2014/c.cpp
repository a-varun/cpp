#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int books[505],order[1005], bko[505];

struct node{
	int id;
	node *next;
	node(){
		next=NULL;
	}
};
	int n,k;

node *head;
int rec(int i){
	//node *tmp=head;while(tmp){cout<<tmp->id<<' ';tmp=tmp->next;}cout<<endl;
	if(i==k) return 0;
	if(head->id == order[i]){
		return rec(i+1);
	}

	node* temp = head;
	int sum=books[head->id];
	while(temp->next && temp->next->id !=order[i] ) {sum+=books[temp->next->id];temp = temp->next;}
	if(!temp->next) return rec(i+1)+1;
	node* t1=temp->next;
	temp->next = temp->next->next;
	t1->next = head;
	head=t1;
	return sum+rec(i+1);
}

int main(){
	cin>>n>>k;
	for(int i=0;i<n;i++) cin>>books[i+1];
	for(int i=0;i<k;i++) cin>>order[i];
	bko[0]=order[0];
	int hash[1006]={-1};
	hash[order[0]]=1;
	int ptr=1;
	for(int i=1;i<n;i++){
		if(hash[order[i]])continue;
		bko[ptr++]=order[i];
		hash[order[i]]=1;
	}
	
	for(int i=1;i<=n;i++){
		if(!hash[i])
			bko[ptr++]=i;
	}
	head = new node();
	head->id = bko[0];
	node *tmp = head;
	for(int i=1;i<n;i++){
		//cout<<bko[i]<<endl;
		tmp->next = new node();
		tmp->next->id = bko[i];
		tmp = tmp->next;
	}
	//tmp=head;while(tmp){cout<<tmp->id<<' ';tmp=tmp->next;}cout<<endl;
	cout<<rec(0)<<endl;
	return 0;
}