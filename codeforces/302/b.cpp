#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int n,m;
	cin>>n>>m;
	int count=0;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(i%2 == j%2) count++;
		}
	}
	if(count>=m){
		cout<<"YES"<<endl;
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(i%2==j%2 && m>0){
					cout<<"L";
					m--;
				}
				else
					cout<<"S";
			}
			cout<<endl;
		}
	}
	else
		cout<<"NO"<<endl;
}