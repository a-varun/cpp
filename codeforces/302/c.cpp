#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[507];
int n,m,b,mod;

int dp[500][500][500];

int rec(int prid, int lined, int bugs){
	if(prid>=n && lined==m){
		return 1;
	}
	if(dp[prid][lined][bugs]!=-1) return dp[prid][lined][bugs];
	if(prid>=n) return 0;
	int tot=0;
	//now, assign some lines to this progrmmr nd continue
	for(int i=0;i+lined<=m;i++){
		int adb = i*ar[prid];
		if(adb+bugs<=b){
			tot+=rec(prid+1, lined+i, bugs+adb);
			tot%=mod;
		}
	}
	return dp[prid][lined][bugs]=tot;

}
int main(){
	memset(dp, -1, sizeof(dp));
	cin>>n>>m>>b>>mod;
	for(int i=0;i<n;i++) cin>>ar[i];
	cout<<rec(0,0,0)<<endl;
}