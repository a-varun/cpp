#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long ll;
typedef pair<int, int> PII;

ll gcd(ll a,ll b){
	if(b==0) return a;
	return gcd(b,a%b);
}
int main(){
	ll l,r;
	cin>>l>>r;
	for(ll i=l;i<=r;i++)
		for(ll j=i+1;j<=r;j++)
			for(ll k=j+1;k<=r;k++)
				if(gcd(i,j)==1&&gcd(j,k)==1&&gcd(i,k)!=1)
				{
					cout<<i<<' '<<j<<' '<<k<<endl;
					return 0;
				}
	cout<<"-1"<<endl;
	return 0;
}