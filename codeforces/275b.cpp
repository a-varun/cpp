#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define pr(c) cout<<c<<endl;
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	LL c1,c2,p1,p2,r,s,tb;
	cin>>c1>>c2>>p1>>p2;
	if(p1<p2){swap(p1,p2);swap(c1,c2);}
	LL tot=0;
	r=int((c1*p1)/(p1-1))-(((c1*p1)%(p1-1))>0?0:1);
	// a takes from 1 to r
	pr(r);
	pr(p2);
	tb = r-int(r/p2);
	pr("adding");
	pr(tb);
	c2+=tb;
	pr("vals are");
	pr(c2);
//	pr(r);pr(p1);pr(p2);pr("dh");
	c2-=(int(r/p1)-int(r/(p1*p2)));
	pr(c2);
	s=int((c2*p2)/(p2-1))-(((c2*p2)%(p2-1))>0?0:1);
	tot=s;
	cout<<tot<<endl;
}