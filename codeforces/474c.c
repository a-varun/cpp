#include <stdio.h>
#include <string.h>


#define MAX 100009
#define mod 1000000007

int n,k;
long long mem[MAX+8];

int next_int()
{
  int n=0;
  char c=getchar_unlocked();
  while(!('0'<=c && c<='9'))
   c=getchar_unlocked();
  while('0'<=c && c<='9')
  {
    n=n*10+c-'0';
    c=getchar_unlocked();
  } 
  return n;
}

int count(int len){
	int su=0;
	if(len==-1) return 0;
	if((len+1)<k) return 0;
	if(mem[len]!=-1) return mem[len];
	su+=count(len-k);
	su+=count(len-1);
	su+=1;
	if(su>mod) su%=mod;
	return mem[len]=su;
}
int main(){
	int ite,i;
	ite=next_int();
	k=next_int();
	memset(mem,0,sizeof(mem));
	mem[k-1]=1;
	for(i=k;i<MAX;i++){
		mem[i]+=mem[i-k]+mem[i-1]+1;
		if(mem[i]>mod) mem[i]%=mod;
	}
	while(ite--){
		int l1,l2;
		l1=next_int();
		l2=next_int();
		long long ans=0;
		for(i=l1;i<=l2;i++){
				ans+=mem[i-1]+1;
				if(ans>mod) ans%=mod;
		}
		printf("%I64d\n",ans);
	}
}