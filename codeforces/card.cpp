#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;




int main()
{
	string ar;
	cin>>ar;
	int n;
	int r,s;
	int dp[ar.length()+2][2];
	memset(dp,0,sizeof(dp));
	dp[0][1]=ar[0]=='(';
	dp[0][0]=0;
	for(int i=1;i<ar.length();i++)
	{
		if(ar[i]=='(')
		{
			dp[i][0]=dp[i-1][0];
			dp[i][1]=dp[i-1][1]+1;
		}
		else
		{
			if(dp[i-1][1]==0)
			{
				dp[i][0]=dp[i-1][0];
				dp[i][1]=dp[i-1][1];
			}
			else
			{
				dp[i][0]=dp[i-1][0]+2;
				dp[i][1]=dp[i-1][1]-1;

			}
		}
	}

	int x,y;
	scanf("%d",&n);
	
	while(n--)
	{
		scanf("%d%d",&x,&y);
		x--;y--;
		if(x==y) cout<<'0'<<endl;

		else if(x==0) {cout<<dp[y][0]<<endl;}
		else
		{
			int rrr=dp[y][0]-dp[x][0]-(dp[x-1][1]*2)+(dp[y][1]*2);
			if(rrr<=0) {cout<<'0'<<endl;}
			else
		cout<<rrr<<endl;
	}}

}