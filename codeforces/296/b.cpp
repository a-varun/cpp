#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	string s,t;
	cin>>s>>t;
	int ham=0;
	REP(i,n) ham+=s[i]!=t[i];
	int ar[26][26]={0};
	for(int i=0;i<n;i++){
		if(s[i]!=t[i]){
			ar[s[i]-'a'][t[i]-'a']=i+1;
			if(ar[t[i]-'a'][s[i]-'a']){
				cout<<ham-2<<endl;
				cout<<i+1<<' '<<ar[t[i]-'a'][s[i]-'a']<<endl;
				return 0;
			}
		}
	}
	int hash[26]={0};
	for(int i=0;i<n;i++){
		if(s[i]!=t[i]){
			hash[t[i]-'a']=i+1;
		}
	}
	for(int i=0;i<n;i++){
		if(s[i]!=t[i]){
			if(hash[s[i]-'a']){
				cout<<ham-1<<endl;
				cout<<hash[s[i]-'a']<<' '<<i+1<<endl;
				return 0;
			}
		}
	}
	cout<<ham<<endl;
	cout<<"-1 -1"<<endl;

	return 0;

}