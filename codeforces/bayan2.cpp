#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int vis[25][25];
int dir[4][2]={{1,0},{-1,0},{0,1},{0,-1}};
int ar[22][22];
int n,m;
void bfs(int i,int j){
	if(i<0||j<0||i>=n||j>=m) return;
	if(vis[i][j]) return;
	vis[i][j]=1;
	for(int k=1;k<=4;k++){
		//cout<<(1<<k)<<' '<<ar[i][j]<<' '<<((1<<k)&ar[i][j])<<endl;
	if(((1<<k)&ar[i][j])){
		bfs(i+dir[k-1][0],j+dir[k-1][1]);
	}
}
}


int main(){
	scanf("%d%d",&n,&m);
	memset(ar,0,sizeof(ar));
	string s;
	cin>>s;
	for(int i=0;i<n;i++){
		if(s[i]=='>'){
			for(int j=0;j<m;j++){
				ar[i][j]|=1<<3;
			}
		}
		else{
			for(int j=0;j<m;j++){
				ar[i][j]|=1<<4;
			}	
		}
	}
	cin>>s;
	for(int i=0;i<m;i++){
		if(s[i]=='^'){
			for(int j=0;j<n;j++){
				ar[j][i]|=1<<2;
			}
		}
		else{
			for(int j=0;j<n;j++){
				ar[j][i]|=1<<1;
			}	
		}
	}
	//for(int i=0;i<n;i++){for(int j=0;j<m;j++)cout<<ar[i][j]<<' ';cout<<endl;}
	bool fl=1;
	for(int i=0;i<n&&fl;i++){
		for(int j=0;j<m&&fl;j++){
			memset(vis,0,sizeof(vis));
			bfs(i,j);
				for(int k=0;k<n;k++)
					for(int l=0;l<m;l++)
						if(!vis[k][l]){
							//cout<<i<<' '<<j<<endl;
							fl=0;
						}
		}
	}
	if(!fl)
		cout<<"NO"<<endl;
	else
		cout<<"YES"<<endl;


}