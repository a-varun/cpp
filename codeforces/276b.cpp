#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

#define maxi 1000000000

int main(){
	LL n,t1,t2;
	cin>>n;
	cin>>t1>>t2;
	t1+=maxi;
	t2+=maxi;
	LL lx = t1, ux = t1, ly = t2, uy = t2;
	for(int i=1;i<n;i++){
		cin>>t1>>t2;
		t1+=maxi;
		t2+=maxi;
		lx=min(lx,t1);
		ux = max(ux, t1);
		ly = min(ly, t2);
		uy = max(uy, t2);
	}
	LL r = uy-ly;
	LL s = ux - lx;
	r = max(r,s);
	cout<<r*r<<endl;

}


