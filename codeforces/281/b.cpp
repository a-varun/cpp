#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	long long int n,t,s1=0,s2=0;
	cin>>n;
	vector<long long int> v1,v2;
	for(long long int i=0;i<n;i++){
		cin>>t;
		if(t>0){
			v1.PB(t);
			s1+=t;
		}
		else{
			v2.PB(t*-1);
			s2+=t*-1;
		}
	}
	if(s1!=s2){
		if(s1>s2)cout<<"first";
		else cout<<"second";
		cout<<endl;
		return 0;
	}
	long long int i;
	for(i=0;i<v1.size()&&i<v2.size();i++){
		if(v1[i]==v2[i])
			continue;
		if(v1[i]>v2[i]){
			cout<<"first"<<endl;
		}
		else
			cout<<"second"<<endl;
		return 0;
	}
	if(i==v1.size()&&i==v2.size()){
		if(t<0) cout<<"second"<<endl;
		else
			cout<<"first"<<endl;
		return 0;
	}
	if(i==v1.size())
		cout<<"second"<<endl;
	else
		cout<<"first"<<endl;
}