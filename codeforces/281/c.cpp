#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	long long int n,t;
	cin>>n;
	vector<long long int> a,b;
	for(long long int i=0;i<n;i++){
		cin>>t;
		a.PB(t);
	}
	long long int m;
	cin>>m;
	for(long long int i=0;i<m;i++){
		cin>>t;
		b.PB(t);
	}
	long long px=0,py=0;
	SORT(a);SORT(b);
	long long int x1=-9999999999999,x2=0,tx1,tx2;
	while(px<n&&py<m){
		while(py<m && a[px]>b[py]){
			py++;
		}
		//cout<<px<<' '<<py<<endl;
		tx1 = 2*px + 3*(n-px);
		tx2 = 2*py + 3*(m-py);
		//cout<<x1-x2<<' '<<tx1-tx2<<endl;
		if((x1-x2)<(tx1-tx2)||(((x1-x2)==(tx1-tx2))&&(x1<tx1))){
			x1=tx1;
			x2=tx2;
		}
		px++;
	}
		tx1 = 2*n;
		tx2 = 2*m;
		if((x1-x2)<(tx1-tx2)||(((x1-x2)==(tx1-tx2))&&(x1<tx1))){
			x1=tx1;
			x2=tx2;
		}

	cout<<x1<<':'<<x2<<endl;
}