#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
#define ZRO 0
#define ONE 1
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
string s;

int ar[1000000][2];
int rec(int pos, int state){
	if(ar[pos][state]!=-1) return ar[pos][state];
	if(pos==s.length()) return 0;
	if(state == ZRO){
		if(s[pos]=='0')
			return ar[pos][state]= min(rec(pos+1,state), rec(pos+1, ONE)+2);
		else{
			return ar[pos][state]=min(rec(pos+1,state)+1, rec(pos+1, ONE)+2);
		}
	}
	else{
		if(s[pos]=='0')
			return ar[pos][state]=min(rec(pos+1,state)+1, rec(pos+1, ZRO)+2);
		else{
			return ar[pos][state]=min(rec(pos+1,state), rec(pos+1, ZRO)+2);
		}

	}
}

int main(){
	memset(ar,-1,sizeof(ar));
	cin>>s;
	int i;
	for(i=0;i<s.length();i++) if(s[i]=='1') break;
	cout<<rec(i,ZRO)<<endl;	
}