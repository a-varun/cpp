#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
#define PR(k) cout<<(k)<<endl;
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int arr[100007];
int make(int a,int b){
	for(int i=a;i<=b;i++){
		arr[i]=b;
	}
}

int main(){
	int n,m,k,i,j;
	cin>>n>>m;
	vector<PII> vtr;
	int ar[n];
	int st=0 , pv = 0, fl;
	for(i=0;i<n;i++){
		cin>>ar[i];
	}
	if(n==1){
		for(int i=0;i<m;i++)cout<<"Yes"<<endl;
			return 0;
	}
	j = 0;
	pv = ar[0];
	j=1;
	i=0;
	while(j<n){
		while(j<n && ar[j]>=ar[j-1]){
			j+=1;
		}
		if(j==n){
			make(i,j-1);
			break;
		}
		while(j<n && ar[j]<=ar[j-1]){
			j+=1;
		}
		if(j==n){
			make(i,j-1);
			continue;
		}
		j-=1;
		make(i,j);
		while(j>0 && ar[j]==ar[j-1]){
			j-=1;
		}
		i=j;
		j+=1;
	}
	//for(i=0;i<n;i++) cout<<arr[i]<<endl;
	while(m--){
		cin>>i>>j;
		i--;
		j--;
		pv=0;
		int fl=0;
		if(arr[i]>=j) fl=1;
		if(fl)cout<<"Yes"<<endl;
		else
			cout<<"No"<<endl;
	}

}
