#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	int n,k;
	cin>>n>>k;
	int ar[n];
	for(int i=0;i<n;i++){
		cin>>ar[i];
	}
	int ptr1=0,ptr2=0,count=-1;
	int tsu=0;
	while(ptr2<n){
		tsu+=ar[ptr2];
		if(tsu<=k){
			count=max(count,ptr2-ptr1+1);
			ptr2+=1;
			continue;
		}
		while(tsu>k){
			tsu-=ar[ptr1];
			ptr1++;
			if(ptr1>ptr2){
				tsu=0;
				break;
			}
		}
		count=max(count,ptr2-ptr1+1);
		ptr2+=1;
	}
	cout<<count<<endl;
}






