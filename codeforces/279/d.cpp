#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	int n;
	cin>>n;
	int ar[n];
	for(int i=0;i<n;i++) cin>>ar[i];
	int ar1[n];
	memset(ar1,0,sizeof(ar));
	ar1[0]=0;
	int x,y;
	for(int i=1;i<n;i++){
		x=-1;y=-1;
		for(int j=0;j<i;j++)
			for(int k=0;k<i;k++)
				if(ar[j]+ar[k]==ar[i])
					if((x==-1)||(min(ar1[x],ar1[y])<min(ar1[j],ar1[k]))){
						x=j;
						y=k;
					}
		ar1[x]=ar1[y]=i;
	}
	int count = 0, mini=0;
	priority_queue<int> pq;
	for(int i=0;i<n;i++){
		pq.push(ar1[i]);
		while(!pq.empty() && pq.top()<=i) pq.pop();
		mini= max(mini,int(pq.size()));
	}
	cout<<mini<<endl;
}