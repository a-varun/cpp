#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

#define MAX 100009
#define mod 1000000007

int n,k;
long long mem[MAX+8];

int next_int()
{
  int n=0;
  char c=getchar();
  while(!('0'<=c && c<='9'))
   c=getchar();
  while('0'<=c && c<='9')
  {
    n=n*10+c-'0';
    c=getchar();
  } 
  return n;
}

int count(int len){
    int su=0;
    if(len==-1) return 0;
    if((len+1)<k) return 0;
    if(mem[len]!=-1) return mem[len];
    su+=count(len-k);
    su+=count(len-1);
    su+=1;
    if(su>mod) su%=mod;
    return mem[len]=su;
}
int main(){
    int ite,i;
    ite=next_int();
    k=next_int();
    memset(mem,0,sizeof(mem));
    mem[k-1]=1;
    for(i=k;i<MAX;i++){
        mem[i]+=mem[i-k]+mem[i-1]+1;
        if(mem[i]>mod) mem[i]%=mod;
    }
    for(int i=1;i<MAX;i++){
    	mem[i]+=mem[i-1];
    	if(mem[i]>mod) mem[i]%=mod;
    }
    //for(int i=0;i<10;i++) cout<<mem[i]<<endl;
    while(ite--){
        int l1,l2;
        l1=next_int();
        l2=next_int();
        long long ans=0;
        if(l1==1){
        	ans=mem[l2-1]+l2;
        }
        else
        	ans=mem[l2-1]-mem[l1-2]+l2-l1+1+mod+mod+mod+mod+mod+mod+mod;
        if(ans>=mod) ans%=mod;
        cout<<ans<<endl;
    }
}