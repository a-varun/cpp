#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int n;
	cin>>n;
	vector<PII > vtr;
	int l=0,r=0;
	REP(i,n){
		int t1,t2;
		cin>>t1>>t2;
		vtr.PB(MP(t1,t2));
		if(t1<0){
			l++;
		}
		else{
			r++;
		}
	}
	SORT(vtr);
	if(l<=r){
		//in this case, the num of apples in left is less than right. So, first go right, then left. So, add all of l, with l+1 number of right
		int sum=0;
		for(int i=0;i<n;i++){
			if(vtr[i].FST <0){
				sum+=vtr[i].SEC;
				continue;
			}
			else{
				sum += vtr[i].SEC;
				if(l<=0) break;
				l--;
			}
		}
		cout<<sum<<endl;
		return 0;
	}
	else{
		//in this case, the num of apples in left is less than right. So, first go right, then left. So, add all of l, with l+1 number of right
		int sum=0;
		for(int i=n-1;i>=0;i--){
			if(vtr[i].FST >0){
				sum+=vtr[i].SEC;
				continue;
			}
			else{
				sum += vtr[i].SEC;
				if(r<=0) break;
				r--;
			}
		}
		cout<<sum<<endl;
		return 0;
	}


}