#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int maxi = 1000000;
int hash[1000000];
int count1[1000000];
int main(){
	int n,val;
	cin>>n;
	REP(i,n){
		cin>>val;
		//cout<<"Starting"<<endl;
		hash[val]++;
		//cout<<val<<endl;
		int cnt = 0;
		int tval = val, tcnt = cnt;

		while((tval*2)<maxi){
			tval*=2;
			tcnt++;
			//cout<<tval<<endl;
			hash[tval]++;
			count1[tval]+=tcnt;
		}
		if(val == 1){
			continue;
		}
		//now remove zeros, add the cnt untill %2 is 1.
		while(val>1){
			while(val%2==0){
				val/=2;
				cnt++;
				//cout<<val<<endl;
				hash[val]++;
				count1[val]+=cnt;
			}

			//now, val ends in a 1. Remove, start appending 0 and inc tcnt
			if(val == 1){
				break;
			}
			val/=2;
			cnt++;
			//cout<<val<<endl;
			hash[val]++;
			count1[val]+=cnt;
			tval = val; tcnt = cnt;
			while((tval*2)<maxi){
				tval*=2;
				tcnt++;
				//cout<<tval<<endl;
				hash[tval]++;
				count1[tval]+=tcnt;
			}
		}
	}
	int maxi1 = 99999999;
	REP(i,maxi){
		if(hash[i]==n){
			if(count1[i]<maxi1)
				maxi1=count1[i];
		}
	}
	cout<<maxi1<<endl;

}