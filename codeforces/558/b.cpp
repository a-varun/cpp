#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int n,t;
	cin>>n;
	VI vtr, vtr1;
	REP(i,n){
		cin>>t;
		vtr.PB(t);
		vtr1.PB(t);
	}
	SORT(vtr);
	//get the max element
	int ele = -1, times = 0, cnt=0, tmp=-1;
	REP(i,n){
		if(vtr[i]!=tmp){
			cnt = 1;
			tmp = vtr[i];
		}
		else{
			cnt++;
		}
		if(cnt>times){
			ele = vtr[i];
			times = cnt;
		}
	}
	VI vals;
	//now get all elements of times count
	cnt=0;
	ele = -1;
	tmp = -1;
	REP(i,n){
		if(vtr[i]!=tmp){
			cnt = 1;
			tmp = vtr[i];
		}
		else{
			cnt++;
		}
		if(cnt==times){
			vals.PB(vtr[i]);
		}
	}
	int ans = 9999999, lp, rp;
	REP(i, vals.size()){
		int element = vals[i];
		int lptr = 0, rptr = n-1;
		while(vtr1[lptr]!=element){
			lptr++;
		}
		while(vtr1[rptr]!=element){
			rptr--;
		}
		if((rptr-lptr) < ans){
			ans = rptr-lptr;
			lp = lptr;
			rp = rptr;
		}			
	}
	cout<<lp+1<<' '<<rp+1<<endl;
}