#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ptr=1;
int fli;

int ar[26][26];
void comp(string s1, string s2){
	int p1=0, p2=0;
	while(p1<s1.length()&&p2<s2.length()){
		if(s1[p1]==s2[p2]){
			p1++;p2++;continue;
		}
		//chars are different
		char g = s1[p1], l=s2[p2];
		ar[g-'a'][l-'a']=1;		
		return;
	}
	if(p1!=s1.length()) fli=1;
	if(p1==s1.length()&&p2==s2.length()) fli=1;
}


int main(){
	fli=0;
	int n;
	cin>>n;
	string s;
	vector<string> vtr;
	REP(i,n){
		cin>>s;
		vtr.PB(s);
	}
	FOR(i,1,vtr.size()){
		comp(vtr[i-1], vtr[i]);
		if(fli){cout<<"Impossible"<<endl;return 0;}
	}
	string sis="";
	int vis[26]={0};
	REP(cntr,26){
		int fl=0,n;
		REP(i,26){
			n=0;
			if(vis[i]) continue;
			REP(j,26){
				if(ar[i][j]==1){n=1;break;}
			}
			if(n) continue;
			//found dependency less char
			sis=char(i+'a')+sis;
			vis[i]=1;
			REP(k,26) ar[k][i]=0;
			fl=1;
			break;
		}
		if(fl) continue;
		cout<<"Impossible"<<endl;return 0;	
	}
	cout<<sis<<endl;
}