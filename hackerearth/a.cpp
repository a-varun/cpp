#include <bits/stdc++.h>
using namespace std;
int dp[2005][2005], m, n;
char a[2005][2005];

int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin>>t;
	while(t--)
	{
		memset(dp, 0, sizeof(dp));
		vector<string> vtr;
		string s;
		int n,m;
		cin>>n>>m;
		for(int i=0;i<n;i++){
			cin>>s;
			vtr.push_back(s);
		}
		int tmax=0;
		for(int i=0; i<n;i++){
			if(i%2==0){
				for(int j=0;j<vtr[0].length(); j++){

					if(vtr[i][j]=='#'){
						dp[i][j]=-100000;
						continue;
					}
					dp[i][j]=vtr[i][j]-'0';
					int maxi;
					if(i==0&&j==0) maxi=0;
					else if(i>0){
						maxi = dp[i-1][j];
						if(j>0) maxi=max(maxi, dp[i][j-1]);
					}
					else{
						maxi=dp[i][j-1];
					}
					dp[i][j]+=maxi;
					tmax=max(tmax, dp[i][j]);
				}
			}
			else{
				for(int j=vtr[0].length()-1;j>=0; j--){
						if(vtr[i][j]=='#'){
							dp[i][j]=-100000;
							continue;
						}
						dp[i][j]=vtr[i][j]-'0';
						int maxi=dp[i-1][j];
						if(j<m-1)
							maxi=max(maxi, dp[i][j+1]);
						dp[i][j]+=maxi;
						tmax=max(tmax, dp[i][j]);
					}

			}
		}
		// int maxi=0;
		// for(int i=0;i<m;i++) maxi=max(maxi, dp[n-1][i]);
		cout<<tmax<<endl;
	}
	return 0;
}