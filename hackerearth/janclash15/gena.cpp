#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;


int main(){

	int ar[100][100];
	memset(ar,-1,sizeof(ar));
	for(int i=0;i<100;i++)
		ar[i][0]=ar[0][i]=1;
	ar[0][0]=0;

	for(int i=1;i<100;i++){
		for(int j=1;j<100;j++){
			int fl = 0;
			int x=i,y=j;
			x--;
			while(x>=0 && y>=0){
				if(ar[x][y]==0){ fl=1;}
				x--;
			}
			x=i;y=j;
			y--;
			while(x>=0 && y>=0){
				if(ar[x][y]==0) { fl=1;}
				y--;
			}
			x=i;y=j;
			x--;y--;
			while(x>=0 && y>=0){
				if(ar[x][y]==0) { fl=1;}
				x--;y--;
			}
			ar[i][j]=fl;
		}
	}
	for(int i=0;i<100;i++){
		for(int j=i+1;j<100;j++){
			if(ar[i][j]==0)
			printf("%d %d\n",i,j);
		}
	
	}
}