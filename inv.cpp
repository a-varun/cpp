#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(long long int i=0; i<(n); i++)
#define FOR(i, a, b) for(long long int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(long long int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(long long int i=(a); i<(b); i+=(c))
 
#define SS ({long long int x;scanf("%d", &x);x;})
#define SI(x) ((long long int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<long long int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<long long int, long long int> PII;
long long int ar[1000009];
long long int ca[10000009];
long long int mergel(long long int x1,long long int y1,long long int x2,long long int y2);
long long int split(long long int lo, long long int hi)
{
    if(lo==hi) return 0;
        long long int mid=(hi+lo)/2;
        long long int x=split(lo,mid);
        long long int y=split(mid+1,hi);
        long long int z=mergel(lo,mid,mid+1,hi);
        return x+y+z;
}
long long int mergel(long long int x1,long long int y1,long long int x2,long long int y2)
{       long long int qwe=y2-x1+1;
        long long int temp=0;
        long long int s1=(y1-x1+1),s2=(y2-x2+1);
        long long int sum=0,pos1=x1,pos2=x2;
        while(1)
        {
                if(pos1==x2)
                {
                        while(pos2<(y2+1))
                        {
                                ca[temp++]=ar[pos2++];
                        }
                        break;
                }
                if(pos2==y2+1)
                {
                        while(pos1<x2)
                        {
                                ca[temp++]=ar[pos1++];
                        }
                        break;
                }
                if(ar[pos1]<=ar[pos2])
                {
                        ca[temp++]=ar[pos1++];
                        continue;
                }
                sum+=x2-pos1;
                ca[temp++]=ar[pos2++];
        }
        long long int t=x1,r=0;
        while(t<=y2)
        {
                ar[t++]=ca[r++];
        }
        return sum;
}
int main()
{int ite;
        cin>>ite;
    for(int rs=1;rs<=ite;rs++){
        cout<<"Case "<<rs<<": ";
        long long int n,k;
        cin>>n>>k;
        for(int i=0;i<n;i++)
        {
            cin>>ar[i];
        }
        long long r=split(0,n-1);
        if(k<r){
            cout<<(r-k)<<endl;
        }
        else if((k-r)%2==0)
            cout<<"0"<<endl;
        else
            cout<<"1"<<endl;
        }
}
