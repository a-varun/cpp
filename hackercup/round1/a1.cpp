#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

#define tot 10000007
int prime[tot];

#define REP(i,n)	for(int i=0;i<n;i++)
#define RE(i,j,n)	for(int i=j;i<n;i++)
using namespace std;
typedef long long LL;
typedef long L;
#define maxi 10000007
int arr[maxi];
int main()
{
	ios::sync_with_stdio(false);
	int t;
	scanf("%d",&t);
	for(LL y=2;y<=maxi;y++)
	{
		if(arr[y]>0)	continue;
		else
		{
			arr[y]=1;
			LL counter=2;
			while(true)
			{
				if((y*counter)<maxi)
				{
					arr[(y*counter)]++;
				}
				else
				{
					break;
				}
				counter++;
			}
		}
	}
	LL a,b,k;
	REP(z,t)
	{
		cin>>a>>b>>k;
		LL count=0;
		RE(i,a,b+1)
		{
			if(arr[i]==k)	count++;
		}
		cout<<"Case #"<<(z+1)<<": "<<count<<endl;
	}
	return 0;
}