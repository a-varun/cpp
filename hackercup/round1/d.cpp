#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define

int main(){
	int ite;
	cin>>ite;
	int c=1;
	while(ite--){
		cout<<"Case #"<<(c++)<<": ";
		int n;
		cin>>n;
		int ar[n+2];
		for(int i=1;i<=n;i++){
			cin>>ar[i];
		}
		int lvl[n+2];
		lvl[0]=0;
		lvl[1]=1;
		for(int i=2;i<=n;i++){
			lvl[i]=lvl[ar[i]]==2?1:2;
		}
		long long count=0;
		for(int i=1;i<=n;i++) count+=lvl[i];
		lvl[1]=2;
		for(int i=2;i<=n;i++){
			lvl[i]=lvl[ar[i]]==2?1:2;
		}
		long long count1=0;
		for(int i=1;i<=n;i++) count1+=lvl[i];

			cout<<min(count,count1)<<endl;
	}
}