#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

struct node{
	node* ptrs[26];
	node(){
		for(int i=0;i<26;i++)
			ptrs[i]=NULL;
	}
};

node *head;

long long addt(string s){
	LL ptr=0,count=0;
	node* h = head;
	while(ptr<s.length() ){
		count++;
		if(h->ptrs[s[ptr]-'a']==NULL) break;
		h = h->ptrs[s[ptr]-'a'];
		ptr++;
	}
	while(ptr<s.length()){
		h->ptrs[s[ptr]-'a'] = new node();
		h = h->ptrs[s[ptr]-'a'];
		ptr+=1;
	}
	return count;
}


int main(){
	int ite;
	cin>>ite;
	int c=1;
	while(ite--){
		cout<<"Case #"<<(c++)<<": ";
		int n;
		head=new node();
		cin>>n;
		string s;
		long long totc = 0;
		for(int i=0;i<n;i++){
			cin>>s;
			totc+= addt(s);
		}
		cout<<totc<<endl;
	}
}