#include "template.cpp"
using namespace std;

char newline;
class data_node
{
	public:
		string name;
		string number;
		vector<string> address;
		data_node(string a, string b, vector<string> c)
		{
			name=a;
			number=b;
			REP(i,5)
				address.PB(c[i]);
		}
		void disp()
		{
			REP(i,20)
				cout<<"=";
			cout<<endl;
			cout<<"Name       :" << name<<endl;
			cout<<"Number     :" << number<<endl;
			cout<<"Address    :" << endl;
			cout<<"Door Number:" << address[0]<<endl;
			cout<<"Street     :" << address[1]<<endl;
			cout<<"Area       :" << address[2]<<endl;
			cout<<"City       :" << address[3]<<endl;
			cout<<"Pincode    :" << address[4]<<endl;
			REP(i,20)
				cout<<"=";
			cout<<endl;
		}
};
	
	
class tri_node
{
	public:
	int end;
	int sum;
	tri_node* next[10];
	data_node* data;
		tri_node()
		{
			end=0;
			sum=0;
			REP(i,10)
				next[i]=NULL;
	 	data=NULL;
	 }
	tri_node(int en, data_node *temp)
	{
		end=1;
		sum=0;
		REP(i,10)
			next[i]=NULL;
		data=temp;
	}
};


class phonebook
{
	public:
		tri_node* head;
		phonebook()
		{
			head=new tri_node();
		}
		void addnum(string num, data_node* node)
		{
			tri_node *temp=head;
			REP(i,num.length()-1)
			{
				if(temp->next[num[i]-'0']==NULL)
				{
					temp->next[num[i]-'0']=new tri_node();
				}

				temp=temp->next[num[i]-'0'];
				temp->sum++;
			}
			temp->next[num[num.length()-1]-'0']=new tri_node(1,node);
		}
		
		void se( tri_node *temp, string s, vector<data_node*> &nums)
		{
			if(temp->end&&s=="")
			{
				nums.PB(temp->data);
				return;
			}
			
			if(s=="")
			{
				return;
			}
			if(s[0]>='0'&&s[0]<='9')
			{
				if(temp->next[s[0]-'0']==NULL)
				{
					return;
				}
				se(temp->next[s[0]-'0'],s.substr(1),nums);
				return;
			}
			REP(i,10)
			{
				
				if(temp->next[i]==NULL)
					continue;
				se(temp->next[i],s.substr(1),nums);
			}
			return;
		}
				
		void search( )
		{
			string number;
			cout<<"Enter the number";
			cin>>number;
			vector<data_node*> nums;
			se(head,number,nums);
			if(nums.size()!=0){
			REP(k,nums.size())
				nums[k]->disp();
			return;
			}
			for(int i=number.length()-1;i>=0;i--)
			{
				string ns=number;
				ns[i]='_';
				se(head,ns,nums);
				if(nums.size()!=0){
				REP(k,nums.size())
					nums[k]->disp();
				return;
				}
			}
			cout<<"Invalid Entry"<<endl;
		}

		void search_dynn()
		{
			cout<<"Enter the number"<<endl;
			getchar();
			system("stty raw");
			char a;
			tri_node* temp=head,*temp1;
			string str1,str2;
			int count=0,flag=0;
			while(a=getchar())
			{
				if(a==char(13))
					break;
				if(!(a>='0'&&a<='9'))
					continue;
				str1+=a;
				REP(j,count+1)
				{
					cout<<" ";
				}
				REP(j,count+1)
				{
					cout<<"\b";
				}
				
				if(temp==NULL||temp->next[a-'0']==NULL)
				{
					temp=NULL;
					continue;
				}
				temp=temp->next[a-'0'];
				count=0;flag=0;
				temp1=temp;
				while(!flag)
				{
					if(temp1->end)
						break;
					for(int i=0;i<10;i++)
					{
						if(temp1->next[i])
						{
							cout<<char(i+'0');
							count++;
							temp1=temp1->next[i];
							flag=0;
							break;
						}
						flag=1;
					}

				}
				REP(i,count)
					cout<<"\b";
			}
			cout<<" \b\b \b\b";
			for(int i=0;i<count;i++)
				cout<<" ";
			system("stty cooked");
			cout<<endl;
			if(temp!=NULL&&temp->end)
			{
				if(temp->data!=NULL)
				(temp->data)->disp();
			}
			else if(temp1!=NULL&&temp1->end)
			{
				if(temp1->data!=NULL)
				(temp1->data)->disp();
			}
		}
		void del_tri(string s)
		{
			tri_node* temp=head;
			REP(i,s.length())
			{
				if(temp->next[s[i]-'0']==NULL||temp->next[s[i]-'0']->sum==1)
				{
					if(temp->next[s[i]-'0']==NULL||temp->next[s[i]-'0']->sum==1)
					{
						if(temp->next[s[i]-'0']==NULL)
							return;
						temp->next[s[i]-'0']=NULL;
						return;
					}
				}
				temp=temp->next[s[i]-'0'];
			}

			temp->end=0;
			return;
		}
		void display_tri()
		{
			tri_node* temp=head;
			while(1)
			{
				REP(i,10)
					cout<<temp->next[i]<<'\t';
				cout<<"e "<<temp->end;
				cout<<endl;
				int n;
				cin>>n;
				if(n==-1) return;
				temp=temp->next[n];
			}
		}
		bool ispresent(string number)
		{
			tri_node* temp=head;
			REP(i,number.length())
			{
				if(temp->next[number[i]-'0']!=NULL)
				{
					temp=temp->next[number[i]-'0'];
				}
				return 0;
			}
			return temp->end==1;
		}
};			 		
		

class word_node
{
	public:
	int end;
	int sum;
	word_node* next[26];
	data_node* data;
	word_node()
	{
		end=0;
		sum=0;
		REP(i,26)
			next[i]=NULL;
	 	data=NULL;
	 }
	word_node(int en, data_node *temp)
	{
		end=1;
		sum=0;
		REP(i,26)
			next[i]=NULL;
		data=temp;
	}
};


class phonebookword
{
	public:
		word_node* head;
		phonebookword()
		{
			head=new word_node();
		}
		void addword(string word, data_node* nodejs)
		{
			word_node *temp=head;
			REP(i,word.length()-1)
			{
				if(temp->next[word[i]-'a']==NULL)
				{
					temp->next[word[i]-'a']=new word_node();
				}
				temp=temp->next[word[i]-'a'];
				temp->sum++;
			}
			temp->next[word[word.length()-1]-'a']=new word_node(1,nodejs);
		}
		
		void se( word_node *temp, string s, vector<data_node*> &nums)
		{
			if(temp->end&&s=="")
			{
				nums.PB(temp->data);
				return;
			}
			
			if(s=="")
			{
				return;
			}
			if(s[0]>='a'&&s[0]<='z')
			{
				if(temp->next[s[0]-'a']==NULL)
				{
					return;
				}
				se(temp->next[s[0]-'a'],s.substr(1),nums);
				return;
			}
			REP(i,26)
			{
				
				if(temp->next[i]==NULL)
					continue;
				se(temp->next[i],s.substr(1),nums);
			}
			return;
		}
				
		void search( )
		{
			string number;
			cout<<"Enter the name";
			cin>>number;
			vector<data_node*> nums;
			se(head,number,nums);
			if(nums.size()!=0){
			REP(k,nums.size())
				nums[k]->disp();
			return;
			}
			for(int i=number.length()-1;i>=0;i--)
			{
				string ns=number;
				ns[i]='_';
				se(head,ns,nums);
				if(nums.size()!=0){
				REP(k,nums.size())
					nums[k]->disp();
				return;
				}
			}
			cout<<"Invalid Entry"<<endl;
		}

		void search_dynn()
		{
			cout<<"Enter the name"<<endl;
			getchar();
			system("stty raw");
			char a;
			word_node* temp=head,*temp1;
			string str1,str2;
			int count=0,flag=0;
			while(a=getchar())
			{
				if(a==char(13))
					break;
				if(!((a>='a'&&a<='z')||(a>='A'&&a<='Z')))
					continue;
				if(a>='A'&&a<='Z')
					a+=32;
				str1+=a;
				REP(j,count+1)
				{
					cout<<" ";
				}
				REP(j,count+1)
				{
					cout<<"\b";
				}
				
				if(temp==NULL||temp->next[a-'a']==NULL)
				{
					temp=NULL;
					continue;
				}
				temp=temp->next[a-'a'];
				count=0;flag=0;
				temp1=temp;
				while(!flag)
				{
					if(temp1->end)
						break;
					for(int i=0;i<26;i++)
					{
						if(temp1->next[i])
						{
							cout<<char(i+'a');
							count++;
							temp1=temp1->next[i];
							flag=0;
							break;
						}
						flag=1;
					}

				}
				REP(i,count)
					cout<<"\b";
			}
			cout<<" \b\b \b\b";
			for(int i=0;i<count;i++)
				cout<<" ";
			system("stty cooked");
			cout<<endl;
			if(temp->end)
			{
				if(temp->data!=NULL)
				(temp->data)->disp();
			}
			else if(temp1->end)
			{
				if(temp1->data!=NULL)
				(temp1->data)->disp();
			}
		}
		void del_word(string s)
		{
			word_node* temp=head;
			REP(i,s.length())
			{
				if(temp->next[s[i]-'a']==NULL||temp->next[s[i]-'a']->sum==1)
				{
					if(temp->next[s[i]-'a']==NULL||temp->next[s[i]-'a']->sum==1)
					{
						if(temp->next[s[i]-'a']==NULL)
							return;
						temp->next[s[i]-'a']=NULL;
						return;
					}
				}
				temp=temp->next[s[i]-'a'];
			}

			temp->end=0;
			return;
		}
		void display_word()
		{
			word_node* temp=head;
			while(1)
			{
				REP(i,10)
					cout<<temp->next[i]<<'\t';
				cout<<"e "<<temp->end;
				cout<<endl;
				int n;
				cin>>n;
				if(n==-1) return;
				temp=temp->next[n];
			}
		}
		bool ispresent(string number)
		{
			word_node* temp=head;
			REP(i,number.length())
			{
				if(temp->next[number[i]-'a']!=NULL)
				{
					temp=temp->next[number[i]-'a'];
				}
				return 0;
			}
			return temp->end==1;
		}
};		

class phonedb
{
	public:
		phonebook numbers;
		phonebookword words;
		void get_user_phonenumber()
		{
			string name,tname;
			cout<<"Enter the Name : ";
			getline(cin,name);
			REP(i,name.length())
			{
				if(name[i]>='a'&&name[i]<='z')
				{
					tname+=name[i];
					continue;
				}
				if(name[i]>='A'&&name[i]<='Z')
				{
					tname+=char(name[i]+32);
					continue;
				}
			}
			if(words.ispresent(tname))
			{
				cout<<"Name Exists"<<endl;
				return;
			}
			string num,tnum;
			cout<<"Enter Phone Number:";
			cin>>num;
			int fl=0;
			while(1)
			{
				fl=0;
				REP(i,num.length())
				{
					if(num[i]>='0'&&num[i]<='9')
						continue;
					fl=1;
					break;
				}
				if(fl)
				{
					cout<<"Invalid Number. Enter Again:";
					cin>>num;
					continue;
				}
				break;
			}
			if(numbers.ispresent(num))
			{
				cout<<"Number already exists"<<endl;
				return;
			}
			vector<string> addr;
			string tell;
			system("stty raw");
			cout<<"Enter address:Door Number:";
			cin>>tell;
			addr.PB(tell);
			REP(i,13+tell.length()+1)
				cout<<" \b\b";
			cout<<"Street Name:";
			cin>>tell;
			addr.PB(tell);
			REP(i,13+tell.length()+1)
				cout<<" \b\b";
			cout<<"Area Name:";
			cin>>tell;
			addr.PB(tell);
			REP(i,11+tell.length()+1)
				cout<<" \b\b";
			cout<<"City Name:";
			cin>>tell;
			addr.PB(tell);
			REP(i,11+tell.length()+1)
				cout<<" \b\b";
			cout<<"Pin Code:";
			cin>>tell;
			addr.PB(tell);
			cout<<" \b\b \b\b \b\b";
			data_node* tempo=new data_node(name,num,addr);
			system("stty cooked");
			cout<<endl;
			numbers.addnum(num,tempo);
			words.addword(tname,tempo);
		}
		void search()
		{
			cout<<"1.Search Dynamic over phone number.\n2.Search Dynamic over word.\n3.Search Query over number.\n4.Search Query over word.\n";
			cout<<"Choose:";
			int n;
			cin>>n;
			switch(n)
			{
				case 1: numbers.search_dynn();break;
				case 2: words.search_dynn();break;
				case 3: numbers.search();break;
				case 4: words.search();break;
			}
			return;
		}
};
int main()
{
	phonedb a;
	a.get_user_phonenumber();
	a.get_user_phonenumber();
	a.get_user_phonenumber();
	a.get_user_phonenumber();
	a.search();
	a.search();
	a.search();
	a.search();
}
