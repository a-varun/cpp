#include<iostream>
#include<string.h>
#include<fstream>
using namespace std;
ofstream f("dictionary.dat",ios::app);
class Words
{
		char* data;
		char *meaning;
		Words *left;
		Words *right;
		public:
			Words(char* w,char* m,Words *l=NULL,Words *r=NULL)
			{
				int len1,len2;
				len1=strlen(w);
				len2=strlen(m);
				data=new char[len1+1];
				meaning=new char[len2+1];
				strcpy(data,w);
				strcpy(meaning,m);
				left=l;
				right=r;
			}
			Words* insert(Words *t,char* val,char *mean)
			{
				if(t==NULL)	return new Words(val,mean,NULL,NULL);
				int check;
				check=strcmp(t->data,val);
				if(check > 0)
				{
					t->left=insert(t->left,val,mean);
				}
				else if(check < 0)
				{
					t->right=insert(t->right,val,mean);
				}
				else if(check==0)
				{
					//cout<<"Word already exists"<<endl;
				}				
				return t;
			}
			int search(Words *t,char *val)
			{
				if(!t)	return 0;
				int check;
				check=strcmp(t->data,val);
				if(check == 0)
				{
					cout<<t->data<<endl;
					cout<<t->meaning<<endl<<endl;
					return 1;
				}
				if(check > 0)
					return search(t->left,val);
				if(check < 0)
					return search(t->right,val);
			}
			void inorder(Words *t)
			{
				if(t!=NULL)
				{
					inorder(t->left);
					//f<<t->data<<endl<<t->meaning<<endl;
					cout<<t->data<<endl<<t->meaning<<endl<<endl;
					
					inorder(t->right);
				}
			}
			int modify(Words *t,char *w,char *m,int choice)
			{
				if(!t)	return 0;
				int check;
				check=strcmp(t->data,w);
				if(check == 0)
				{
					if(choice == 1)
					{
						int len;
						len=strlen(m);
						t->meaning=new char[len+1];
						strcpy(t->meaning,m);
					}
					else if(choice==2)
					{
						int len1,len2;
						len1=strlen(m);
						len2=strlen(t->meaning);
						char a[len2+1];
						strcpy(a,t->meaning);
						t->meaning=new char[len1+len2+2];
						strcat(a,",");
						strcat(a,m);
						strcpy(t->meaning,a);
					}
					return 1;
					
				}
				if(check > 0)
					return search(t->left,w);
				if(check < 0)
					return search(t->right,w);
		
			}
			void writeWord(Words *t)
			{
				if(t!=NULL)
				{			
					//f<<t->data<<"\t"<<t->meaning<<endl;
					f<<t->data<<"\t";
					int i=0;
					
					while(t->meaning[i]!='\0')
					{
							//cout<<"works"<<endl;
							//cout<<t->meaning[i]+1;
							if(meaning[i]+1==33)
							{
								meaning[i]='@';
							}
							i++;
							
					}
					f<<meaning<<endl;
					writeWord(t->left);
					writeWord(t->right);
				}
			}
};

class Index
{
	char letter;
	Index *lefti;
	Index *righti;
	public:
		Words *t;
		Index(char le,Index *l =NULL,Index* r =NULL, Words *tr=NULL)
		{
			letter=le;
			lefti=l;
			righti=r;
			t=tr;
		 }
		Index* LL(Index *k1)
		{
			Index* k2=k1->lefti;
			Index* t=k2->righti;
			k2->righti=k1;
			k1->lefti=t;
			return k2;
		}
		Index* RR(Index *k1)
		{
			Index* k2=k1->righti;
			Index* t=k2->lefti;
			k2->lefti=k1;
			k1->righti=t;
			return k2;
		} 
		Index* LR(Index *k1)
		{
			k1->lefti=RR(k1->lefti);
			return LL(k1);
		}
		Index* RL(Index *k1)
		{
			k1->righti=LL(k1->righti);
			return RR(k1);
		}
		int height(Index *i)
		{
			if(!i)	return 0;
			int l=height(i->lefti);
			int r=height(i->righti);
			return (l>r)?(l+1):(r+1);
		}
		Index* create(Index *i,char val)
		{
			if(i==NULL)	return new Index(val,NULL,NULL,NULL);
			else if(i->letter>val)
			{
				i->lefti=create(i->lefti,val);
				if(height(i->lefti)-height(i->righti)==2)
				{
					if(i->lefti->letter>val)	i=LL(i);
					else	i=LR(i);
				}
			}
			else if(i->letter<val)
			{
				i->righti=create(i->righti,val);
				if(height(i->lefti)-height(i->righti)==-2)
				{
					if(i->righti->letter<val)	i=RR(i);
					else	i=RL(i);
				}
			}
			return i;
		}
		Index *insertWord(Index *i,char* val,char *mean)
		{
			if(!i)	return i;
			Index *temp=i;
			temp=find(i,val[0]);
			temp->t=temp->t->insert(temp->t,val,mean);
			return i;
		}
		Index *find(Index *i,char a)
		{
			if(!i)
				return NULL;
			if(i->letter==a)
			{
				return i;
			}
			if(i->letter>a)
				return find(i->lefti,a);
			else if(i->letter<a)
				return find(i->righti,a);
		}
		void display(Index *i)
		{
			if(i)
			{
				display(i->lefti);
				i->t->inorder(i->t);
				display(i->righti);
			}
		}
		
		void writedata(Index *i)
		{
			if(i)
			{
				i->t->writeWord(i->t);
				writedata(i->lefti);
				writedata(i->righti);
			}
			//f.close();
		}
		
		
};

int main()
{
//	char sf[20];
//	int flg=1;
	char a1[20],b1[50];
	ifstream fp1("dictionary.dat");
	//char a;
	//fp>>a;
	//fp.close();
	//f.close();
	//ofstream f("dictionary.dat",ios::out);
	//cout<<a;
	Index* i,*temp;
	i=new Index('m',NULL,NULL,NULL);
	for(int j=97;j<123;j++)
	{
		i=i->create(i,j);
	}
	char a;
	while(!fp1.eof())
	{
		int number=0;
		fp1>>a1>>b1;
		number=strlen(b1);
		while(number--)
		{
			if(b1[number]=='@')	b1[number]=' ';
		}
		
		//cout<<a1<<endl<<b1<<endl;
		
		i=i->insertWord(i,a1,b1);
		
	}
	fp1.close();
	int ch,choice;
	char s1[20];
	char s2[50];
	while(1)
	{
		cout<<"\n1.Add a word\n2.Search for a word\n3.Modify the meaning\n4.Display all words\nPress any other key to exit"<<endl;
		cout<<"Enter ur choice:";
		
		cin>>ch;
		//scanf(" %[^\n]s",sf);
		//cout<<sf;
		//if(ch==1)
		
			
				
		switch(ch)
		{
			case 1:
				cout<<"Enter the word to be added:";
				cin>>s1;
				cout<<"Enter the meaning:";
				scanf(" %[^\n]s",s2);
				i=i->insertWord(i,s1,s2);
				break;
			case 2:
				cout<<"Enter the word to be searched:";
				cin>>s1;
				temp=i->find(i,s1[0]);
				if(temp->t->search(temp->t,s1)==0)	cout<<"Word not found"<<endl;
				break;
			case 3:
				cout<<"\t1.Replace existing meaning\n\t2.Add aditional meaning\n\tEnter your choice :";
				cin>>choice;
				switch(choice)
				{
						case 1:
						case 2:
							cout<<"Enter the word:";
							cin>>s1;
							cout<<"Enter the meaning:";
							scanf(" %[^\n]s",s2);
							temp=i->find(i,s1[0]);
							temp->t->modify(temp->t,s1,s2,choice);
							break;
						default:
							cout<<"Wrong choice"<<endl;
				}
					break;
				
			case 4:
				i->display(i);
				break;
			default:
			//	flg=0;
				
				i->writedata(i);
				//cout<<"default";
				f.close();
				goto end;
			}
	}
	end:
	return 0;
}

