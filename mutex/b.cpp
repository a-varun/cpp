#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int ar[9][9];
void print();
int check(int x,int y,int val){
	for(int i=0;i<9;i++){
		if(y==i) continue;
		if(ar[x][i]==val){
			 return 0;}
	}
	for(int i=0;i<9;i++){
		if(x==i) continue;
		if(ar[i][y]==val) {
			return 0;}
	}
	int sp=(x/3)*3,yp=(y/3)*3;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			if(((i+sp)==x)&&((j+yp)==y)) continue;
			if(ar[i+sp][j+yp]==val) { return 0;}
		}
	}
	return 1;
}

int rec(int x,int y){
	//cout<<x<<' '<<y<<endl;
	if(x>=9) return 1;
	if(y>=9) return rec(x+1,0);
	if(ar[x][y]!=-1) return rec(x,y+1);
	for(int i=1;i<=9;i++){
		if(check(x,y,i)){
			ar[x][y]=i;
			if(rec(x,y+1)) return 1;
			ar[x][y]=-1;
		}
	}
	return 0;
}

void print(){
	for(int i=0;i<9;i++){
		for(int j=0;j<9;j++){
			cout<<(char)(ar[i][j]==-1?'.':ar[i][j]+'0');
			if((j+1)%3==0) cout<<' ';
		}
		if((i+1)%3==0) cout<<'\n';
		cout<<'\n';
	}
}
int main(){
	string str;
	cin>>str;
	for(int i=0;i<9;i++) 
		for(int j=0;j<9;j++)
			ar[i][j]=str[i*9+j]=='.'?-1:str[i*9+j]-'0';
	if(rec(0,0))
		print();
	else
		printf("False array");
}