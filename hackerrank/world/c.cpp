#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define int long long

int globs = -1;
int cost[500005];
vector<vector<int> > vtr(50005, vector<int>() );
int ar[500005], vis[500005];
int tot;

int bfss(int n){
	queue<int> stk;
	stk.push(n);
	int p=0;
	while(!stk.empty()){
		n = stk.front();
		stk.pop();
		if(vis[n]!=0) continue;
		vis[n]=1;
		ar[p++] = n;
		REP(i,vtr[n].size()){
			stk.push(vtr[n][i]);
		}
	}
	memset(vis,-1,sizeof(vis));
	for(int i=p-1;i>=0;i--){
		//child sum
		int tsum = 0, maxi = -1;
		n = ar[i];
		REP(j, vtr[n].size()){
			int child = vtr[n][j];
			if(vis[child] == -1){
				continue;
			}
			maxi = max(maxi, vis[child]);
			tsum += vis[child];
		}
		maxi = max(maxi, tot -(tsum + cost[n]));
		globs = max(globs, tot-maxi);
		vis[n] = tsum + cost[n];

	}
	return 0;
}
#undef int
int main(){
	#define int long long
	int n,t1,t2;
	cin>>n;
	REP(i,n){
		cin>>cost[i+1];	
		tot += cost[i+1];
	}
	REP(i,n-1){
		cin>>t1>>t2;
		vtr[t1].push_back(t2);
		vtr[t2].push_back(t1);
	}
	// REP(i,1+n){cout<<i<<"   ";REP(j,vtr[i].size()) cout<<vtr[i][j]<<' ';cout<<endl;}
	int root;
	bfss(1);
	cout<<globs<<endl;
}