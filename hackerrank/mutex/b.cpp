#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int ar[105][105];
int n,m,k,st;
int sx,sy;


int isvalid(int x,int y){
	if(x<0||x>=n||y<0||y>=m) return 0;
	return 1;
}

int nooptn(int x,int y){
	int s=0;
	if(isvalid(x+1,y))
	s+=ar[x+1][y]!=-1;
	if(isvalid(x-1,y))
	s+=ar[x-1][y]!=-1;
	if(isvalid(x,y+1))
	s+=ar[x][y+1]!=-1;
	if(isvalid(x,y-1))
	s+=ar[x][y-1]!=-1;
	return s;
}
void dobfs(int x,int y, int tk){
	if(x<0||x>=n||y<0||y>=m) return;
	if(ar[x][y]==-2){
		if(tk==k)
			st=1;
		return;
	}
	if(ar[x][y]==-1) return;
	//cout<<x<<' '<<y<<' ';
	int i=nooptn(x,y);
	//cout<<i<<endl;
	if(i>=2)
		tk+=1;
	ar[x][y]=-1;
	dobfs(x+1,y,tk);dobfs(x-1,y,tk);dobfs(x,y+1,tk);dobfs(x,y-1,tk);
	ar[x][y]=0;
}
int main(){
	int ite;
	cin>>ite;
	while(ite--){
		memset(ar,0,sizeof(ar));
		st=0;
		cin>>n>>m;
		string s;
		for(int i=0;i<n;i++){
			cin>>s;
			for(int j=0;j<m;j++){
				if(s[j]=='X')
					ar[i][j]=-1;
				if(s[j]=='M'){
					sx=i;
					sy=j;

				}
				if(s[j]=='E'){
					ar[i][j]=-2;
				}

			}
		}
		cin>>k;
		dobfs(sx,sy,0);
		cout<<(st==0?"no":"yes")<<endl;


	}

}