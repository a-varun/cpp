#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main()
{
	int queeen[8]={-1};
	memset(queeen, -1, sizeof(queeen));
	string s[8];
	REP(i,8)
	{
		cin>>s[i];
	}	
	
	REP(i,8)
	{
		REP(j,8)
		{
			if(s[i][j]!='*')	continue;
			REP(k,8)
			{
				if(s[i][k]=='*' && k!=j )
				{
					cout<<"invalid"<<endl;
					return 0;
				}
			}
			REP(k,8)
			{
				if(s[k][j]=='*' && i!=k)
				{
					cout<<"invalid"<<endl;
					return 0;	
				}
			}
		}
	}

	REP(i,8)
	{
		REP(j,8)
		{
			if(s[i][j]=='*')
			{
				queeen[j]=i;
			}
		}
	}
	REP(i,8)
	{
		for(int j=i+1;j<8;j++)
		{
			if(abs(i-j)==abs(queeen[i]-queeen[j]))
			{
				cout<<"invalid"<<endl;
				return 0;
			}
		}
	}
	cout<<"valid"<<endl;

}
