    #include <bits/stdc++.h>
    using namespace std; 
     
    #define REP(i, n) for(int i=0; i<(n); i++)
    #define PB(x) push_back(x)
    #define MP(a,b) make_pair(a, b)
    #define SORT(a) sort(a.begin(),a.end())
    #define V vector
     
    #define TRACE
     
    #ifdef TRACE
    #define trace1(x)                cerr << #x << ": " << x << endl;
    #define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
    #define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
    #define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
    #define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
    #define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
     
    #else
     
    #define trace1(x)
    #define trace2(x, y)
    #define trace3(x, y, z)
    #define trace4(a, b, c, d)
    #define trace5(a, b, c, d, e)
    #define trace6(a, b, c, d, e, f)
     
    #endif
     
     
    typedef V<int> VI;
    typedef long long LL;
    typedef pair<int, int> PII;
    typedef vector< PII > VPII;
    int s[3];
    
    int findit(int mask) {
    	int vtr[3];
    	memset(vtr, 0, sizeof vtr);
    	REP(i, 9) {
    		if(mask&(1<<i)) {
    			int x = i/3;
    			int y = i%3;
    			vtr[x] ^= (1<<y); 
    			if(x>0) {
    				vtr[x-1] ^= (1<<y);
    			}
    			if(x < 2 ) {
    				vtr[x+1] ^= (1<<y);
    			}
    			if(y>0) {
    				vtr[x] ^= (1<<(y-1));
    			}
    			if(y < 2) {
    				vtr[x] ^= (1<<(y+1));
    			}
    		}
    	}
    	// if(mask==17){
    	// 	REP(i,3){
    	// 		cout<<vtr[i]<<endl;
    	// 	}
    	// 	cout<<"_"<<endl;
    	// }
		REP(i, 3) {
			if(vtr[i] != s[i]) return -1;	
		}
		return __builtin_popcount(mask);
    } 
     
    void solve(int testcase) {
    	memset(s, 0, sizeof s);
     	string te[3];
     	REP(i, 3){
     		cin >> te[i];
     		REP(j, 3) {
     			if(te[i][j] == '*') {
     				s[i] |= (1 << j);
     			}
     		}
     	}
     	
     	int ans = INT_MAX;
     	for(int i=0; i<(1<<10); i++ ) {
     		int res = findit(i);
     		if(res == -1) continue;
     		ans = min(ans, res);
     	}
     	cout << ans << endl;
    }  
     
    void doit() {
    	int ite;
    	cin >> ite;
    	for(int i=1; i<=ite; i++ ) {
    		solve(i);
    	}
    }
     
    int main() {
    	ios::sync_with_stdio(false);
    	doit();
    	return 0;
    }