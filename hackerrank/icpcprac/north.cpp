#include <bits/stdc++.h>
using namespace std; 
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define V vector
 
#define TRACE
 
#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
 
#else
 
#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
 
#endif
 
 
typedef V<int> VI;
typedef long long LL;
typedef pair<int, int> PII;
typedef vector< PII > VPII;

int ar[209][2];
int dp[209][2][209];
 
void solve(int testcase) {
 	int n, k;
 	cin >> n >> k;
 	REP(i, n) {
 		cin >> ar[i][0] >> ar[i][1];
 	}
 	dp[0][0][1] = ar[0][0];
 	dp[0][1][1] = ar[0][1];
 	for(int i=2; i<=k; i++ ) {
 		dp[0][0][i] = INT_MAX;
 		dp[0][1][i] = INT_MAX;
 	}
 	for(int i=1; i<n; i++ ) {
 		for(int j=0; j<2; j++ ) {
 			int l;
 			for(l=1; l<=min(i, k); l++ ) {
 				int block = i != 1 ? min(dp[i-1][j][l-1], dp[i-2][j^1][l-1]) : dp[i-1][j][l-1];
 				int nb = min(dp[i-1][j][l], dp[i-1][j^1][l]);
 				dp[i][j][l] = min(block, nb);
 			}
 			for(;l<=k; l++ ) {
 				dp[i][j][l] = INT_MAX;
 			}
 		}
 	}
 	REP(i, n){ REP(j, 2) { cout << i << " " << j << endl; REP(l, k) cout << dp[i][j][l] << " "; cout << endl; } cout << endl; } cout << endl;
 	cout << min(dp[n-1][0][k], dp[n-1][1][k]) << endl;
}  
 
void doit() {
	int ite;
	cin >> ite;
	for(int i=1; i<=ite; i++ ) {
		solve(i);
	}
}
 
int main() {
	ios::sync_with_stdio(false);
	// doit();
	solve(0);
	return 0;
}