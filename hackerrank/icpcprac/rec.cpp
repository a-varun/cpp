#include <bits/stdc++.h>
using namespace std; 
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define V vector
 
#define TRACE
 
#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
 
#else
 
#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
 
#endif
 
 
typedef V<int> VI;
typedef long long LL;
typedef pair<int, int> PII;
typedef vector< PII > VPII;

int ar[209][2];
int n, k;
int dp[202][2][202];
int doit(int i, int col, int taken) {
	if(dp[i][col][taken]!=-1) return dp[i][col][taken];
	if(i>=n){
		if(taken>=k) return 0;
		return 999999;
	}
		
	if(taken>=k) return 0;

	int tak = ar[i][col] + min( doit(i+1, col, taken+1), doit(i+2, col^1, taken+1) );
	int nt = min(min( INT_MAX, doit(i+1, col, taken)), doit(i+1, col^1, taken));
	return dp[i][col][taken]=min(tak, nt);
}
 
void solve(int testcase) {
 	while(1){
 	cin >> n >> k;
 	if(n==k && k==0) return;	
 	memset(dp,-1,sizeof(dp));
 	

 	int sum = 0;
 	REP(i, n) {
 		cin >> ar[i][0] >> ar[i][1];
 		sum += ar[i][0] + ar[i][1];
 	}
 	cout << sum - min(doit(0, 0, 0), doit(0,1,0)) << endl;	
 	}
}  
 
void doit() {
	int ite;
	cin >> ite;
	for(int i=1; i<=ite; i++ ) {
		solve(i);
	}
}
 
int main() {
	ios::sync_with_stdio(false);
	//doit();
	solve(0);
	return 0;
}