    #include <bits/stdc++.h>
    using namespace std; 
     
    #define REP(i, n) for(int i=0; i<(n); i++)
    #define PB(x) push_back(x)
    #define MP(a,b) make_pair(a, b)
    #define SORT(a) sort(a.begin(),a.end())
    #define V vector
     
    #define TRACE
     
    #ifdef TRACE
    #define trace1(x)                cerr << #x << ": " << x << endl;
    #define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
    #define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
    #define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
    #define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
    #define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
     
    #else
     
    #define trace1(x)
    #define trace2(x, y)
    #define trace3(x, y, z)
    #define trace4(a, b, c, d)
    #define trace5(a, b, c, d, e)
    #define trace6(a, b, c, d, e, f)
     
    #endif
     
     
    typedef V<int> VI;
    typedef long long LL;
    typedef pair<int, int> PII;
    typedef vector< PII > VPII;
     
     
    void solve(int testcase) {
    	int n,k;
    	cin>>n>>k;
    	int ar[k+3];
    	ar[0]=0;
    	ar[k+1]=n;
    	REP(i,k)cin>>ar[i+1];
    	int hash[102]={0};
    	REP(i,k+2){
    		for(int j=i+1;j<k+2;j++){
    			int tmp = ar[j]-ar[i];
    			hash[tmp]=1;
    		}
    	}
    	int cou=0;
    	REP(i,102) {if(hash[i])cout<<i<<' ';}
    	cout<<endl;

    }  
     
    void doit() {
    	int ite;
    	cin >> ite;
    	for(int i=1; i<=ite; i++ ) {
    		solve(i);
    	}
    }
     
    int main() {
    	ios::sync_with_stdio(false);
    	//doit();
    	solve(0);
    	return 0;
    }