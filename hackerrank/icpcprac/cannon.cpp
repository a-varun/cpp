#include <bits/stdc++.h>
using namespace std; 
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define V vector
 
#define TRACE
 
#ifdef TRACE
#define trace1(x)                cerr << #x << ": " << x << endl;
#define trace2(x, y)             cerr << #x << ": " << x << " | " << #y << ": " << y << endl;
#define trace3(x, y, z)          cerr << #x << ": " << x << " | " << #y << ": " << y << " | " << #z << ": " << z << endl;
#define trace4(a, b, c, d)       cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << endl;
#define trace5(a, b, c, d, e)    cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << endl;
#define trace6(a, b, c, d, e, f) cerr << #a << ": " << a << " | " << #b << ": " << b << " | " << #c << ": " << c << " | " << #d << ": " << d << " | " << #e << ": " << e << " | " << #f << ": " << f << endl;
 
#else
 
#define trace1(x)
#define trace2(x, y)
#define trace3(x, y, z)
#define trace4(a, b, c, d)
#define trace5(a, b, c, d, e)
#define trace6(a, b, c, d, e, f)
 
#endif
 
 
typedef V<int> VI;
typedef long long LL;
typedef pair<int, int> PII;
typedef vector< PII > VPII;
 
double dp[103][103];
void solve(int testcase) {
	double ar[103][2];
	cin>>ar[0][0]>>ar[0][1];
	cin>>ar[1][0]>>ar[1][1];
	int n;
	cin>>n;
	REP(i,n) cin>>ar[i+2][0]>>ar[i+2][1];
	REP(i,103)REP(j,103) dp[i][j]= 	DBL_MAX;
	n+=2;
	REP(i,n){
		dp[i][i]=0;
	}
	REP(i,n){
		REP(j,n){
			if(i==j) continue;
			double dist;
			//dist stores min dist from i to j
			dist = sqrt(   pow((ar[i][0]-ar[j][0]),2) + pow((ar[i][1]-ar[j][1]),2) );
			// cout<<i<<' '<<j<<' '<<dist<<endl;
			double twalk, tcan;
			if(i>1)
				tcan = 2+ (abs(dist-50))/5.;
			else
				tcan = DBL_MAX;
			twalk = dist/5.;
			dp[i][j]=min(tcan,twalk);
		}
	}
	// REP(i,n){REP(j,n)cout<<dp[i][j]<<' ';cout<<endl;}
	// cout<<"________________________________"<<endl;
	REP(k,n){
		REP(i,n)
			REP(j,n){
				if((dp[i][k]+dp[k][j])<dp[i][j]){
					dp[i][j]=dp[i][k]+dp[k][j];
				}
			}
	}
	// REP(i,n){REP(j,n)cout<<dp[i][j]<<' ';cout<<endl;}
	cout<<dp[0][1]<<endl;
}  
 
void doit() {
	int ite;
	cin >> ite;
	for(int i=1; i<=ite; i++ ) {
		solve(i);
	}
}
 
int main() {
	ios::sync_with_stdio(false);
	//doit();
	solve(0);
	return 0;
}