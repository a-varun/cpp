#include <bits/stdc++.h>
using namespace std;

int dp[100][500000];
int power[100][500000];
int n,m;
inline int inp(){int n=0,s=1,c=getchar_unlocked();if(c=='-')s=-1;while(c<48)c=getchar_unlocked();while(c>47)n=(n<<3)+(n<<1)+c-'0',c=getchar_unlocked();return n*s;}

int rec(int lvl, int person){
    if(lvl == n-1) return 0;
    if(dp[lvl][person]!=-1) return dp[lvl][person];
    int mini = 9999999, bultp = power[lvl][person]/1004, tem;
    for(int i=0;i<m;i++){
        tem = power[lvl+1][i]%1004;
        if(bultp>=tem){
            mini = min(mini, rec(lvl+1,i));   
        }
        else
            mini = min(mini, rec(lvl+1, i) + (tem - bultp));
    }
    return dp[lvl][person] = mini;
}

int main() {
    ios::sync_with_stdio(false);
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int ite;
    ite=inp();
    while(ite--){
        memset(dp,-1,sizeof(dp));
        n=inp();m=inp();
        for(int i=0;i<n;i++)
            for(int j=0;j<m;j++)
                power[i][j]=inp();
        int t;    
        for(int i=0;i<n;i++)
            for(int j=0;j<m;j++){
                t=inp();
                power[i][j] += t*1004; 
            }
        int bulletp,mini,powerp,tmp;
        for(int i=0;i<m;i++) dp[n-1][i]=0;
        int tmpar[1000];
        for(int i=0;i<m;i++){
            
        }


        for(int i=n-2;i>=0;i--){
            for(int j=0;j<m;j++){
                mini = 99999999;bulletp = power[i][j]/1004;
                for(int k=0;k<m;k++){
                    powerp = power[i+1][k]%1004;
                    tmp = dp[i+1][k];
                    mini = min(mini, (powerp<=bulletp) ? tmp:(powerp-bulletp)+tmp);
                }
                dp[i][j]=mini;
            }
        }
        mini=999999;
        for(int i=0;i<m;i++){
            mini=min(mini, power[0][i]%1004 + dp[0][i]);
        }
        cout<<mini<<endl;


        cout<<mini<<endl;        
    }
    return 0;
}
