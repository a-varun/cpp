		#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <string.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int myf(long long a,long long b)
{
	return a<b;
}

int main()
{
	long long n,m;
	cin>>n>>m;
	long long ar1[n];
	for(int i=0;i<n;i++)
	{
		cin>>ar1[i];
	}
	sort(ar1, ar1+n,myf);
	long long ar[n][n];
	memset(ar,0,sizeof(ar));
	ar[0][0]=0;
	for(int i=1;i<n;i++)
	{
		long long int t1=0,t2=0;
		for(int k=0;k<i;k++)
		{
			t1+=abs(ar1[i]-ar1[k]);
			ar[i][k]=abs(ar1[i]-ar1[k]);
		}
		ar[i][i]=ar[i-1][i-1]+t1;
	}
	REP(i,n){REP(j,n)cout<<ar[i][j]<<'\t';cout<<endl;}
	long long imin=ar[m-1][m-1];
	for(int k=m;k<n;k++)
	{
		imin=min(imin, ar[k][k]-ar[k-m+1][k-m+1]);
	}
	cout<<imin<<endl;

}