
class LinkedTree;
LinkedTree *ci,*ar,*st,*na;
template <class T>
class DLL
{
	struct node
	{
		int freq;
		T data;
		node *next;
		node *prev;
		node(T d,node *n,node *p)
		{	
			freq = 0;
			data = d;
			next = n;
			prev = p;
		}
	};
	node *head;
public:
	DLL()
	{
		head = NULL;
	}
	void create()
	{
		head = NULL;
	}
	void insert(T d)
	{
		node *t=head;
		if(!t)
		{
			head = new node(d,NULL,NULL);
			return;
		}
		while(t->next)
			t=t->next;
		t->next=new node(d,NULL,t);
	}
	T search(string d)
	{
		node *temp=head;
		while(temp)
		{	
			if(temp->data->data == d)
				return temp->data;
			temp = temp->next;
		}
		cout<<"returning null value...."<<endl;
		
		return NULL;
	}
	void print()
	{
		node *t=head;
		while(t)
		{
			cout<<t->data->data<<endl;
			t = t->next;
		}
	}
	void deleteData(T da)
	{
		node *t=head;
		if(t->data == da)
		{
			head = head->next;
			head->prev = NULL;
			return;
		}
		while(t)
		{
			if(t->data == da)
			{
				if(t->next)
				{
					t=t->prev;
					t->next = t->next->next;
					t->next->prev = t;
				}
				else
				{
					t=t->prev;
					t->next = NULL;
				}
				break;
			}
			else
			{
				t=t->next;
			}
		}
	}
	void get()
	{
		vector<string> v;
		node *t=head;
		cout<<"the values are.."<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		while(t)
		{
			cout<<t->data->data<<endl;
			t=t->next;
		}
	}
};

class LinkedTree
{
public:
	string data;
	DLL<LinkedTree*> link;
	DLL<data_node*> the_link;
	LinkedTree()
	{
		link.create();
		the_link.create();
	}
	LinkedTree(string da)
	{
		data = da;
		link.create();
	}
	void insert(data_node* dn)
	{
		string s;
		s=dn->address[3];
		LinkedTree *city = link.search(s);
		if(!city)
		{
			city = new LinkedTree(s);
			link.insert(city);
		}
		s=dn->address[2];
		LinkedTree *area = city->link.search(s);
		if(!area)
		{
			area = new LinkedTree(s);
			city->link.insert(area);
		}
		s=dn->address[1];
		LinkedTree *street = area->link.search(s);
		if(!street)
		{
			street = new LinkedTree(s);
			area->link.insert(street);
		}
		LinkedTree *name = street->link.search(s);
		street->the_link.insert(dn->name);
	}
	void searchOne()
	{	
		string s;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		cout<<"Integerated searching......"<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		link.get();	
		cout<<"Enter the city name.."<<endl;
		cin>>s;
		ci = link.search(s);
		cout<<"The areas in the given city.."<<endl;
		if(ci)
		{
			ci->link.get();
			cout<<"Enter the area name.."<<endl;
			cin>>s;
			ar = ci->link.search(s);
			if(ar)
			{
				cout<<"The streets in the given area are..."<<endl;	
				ar->link.get();
				cout<<"Enter the street name.."<<endl;
				cin>>s;
				st = ar->link.search(s);
				cout<<"The people in the street are.."<<endl;
				if(st)
					st->link.get();
			}
		}
	}
};


int main()
{
    int val;
/*	DLL<int> d;
	d.insert(10);
	d.insert(20);
	d.insert(30);
	d.print();
	d.deleteData(20);
	d.print();*/
	LinkedTree *li=new LinkedTree();
	li->insert();
	li->insert();
	li->insert();
	li->searchOne();
	cin>>val;
	getch();
	return 0;
}
