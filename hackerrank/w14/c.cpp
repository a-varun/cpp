#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int dp[2000][2000];
int ar[2000][2000];
int maxc[2000];

int main() {
    int build, height,I,t,t1;
    cin>>build>>height>>I;

	REP(i,build){
		cin>>t;
		REP(j,t){
			cin>>t1;
			t1--;
			ar[t1][i]++;
		}
	}
	int maxi=-1;
	REP(i,build){
		dp[height-1][i]=ar[height-1][i];
		maxi=max(maxi,dp[height-1][i]);
	}
	maxc[height-1]=maxi;
	for(int i=height-2;i>=0;i--){
		maxi=-1;
		REP(j,build){
			int tmp = dp[i+1][j];
			if((i+I)<height)
				tmp=max(tmp,maxc[i+I]);
            tmp+=ar[i][j];
			dp[i][j]=tmp;
			maxi=max(maxi,tmp);
		}
		maxc[i]=maxi;
	}
    //REP(i,height){
    //    REP(j,build) cout<<ar[i][j]<< ' ';
    //    cout<<endl;
    //}
	cout<<maxc[0]<<endl;

    return 0;
}
