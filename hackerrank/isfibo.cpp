#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define maix 1000000007


map < pair<int,int> , long long int> mp;

long long ncr(int n,int m)
{
	long long a=1,b=1;
	int x,y;
	for(x=(n+1),y=1;x<=(m+n)&&y<=n;x++,y++)
	{
		a*=x;
		b*=y;
		if(a%b==0)
		{
			a=a/b;
			b=1;
		}
		if(a>=maix)
		{
			a%=maix;
		}
		if(a==0||b==0) return 0;

	}
	return a/b;
}

int main()
{
	int n1;
	scanf("%d",&n1);
	while(n1--)
	{
		int n,r;
		scanf("%d%d",&n,&r);
		printf("%lld\n",ncr(max(n,r)-1,min(n,r)-1));
	}
}