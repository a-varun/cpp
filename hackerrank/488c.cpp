#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
int main(){
	int mh,ma,md,yh,ya,yd,ch,ca,cd;
	cin>>yh>>ya>>yd>>mh>>ma>>md>>ch>>ca>>cd;
	int mr = ya-md;
	int yr = ma-yd;
	mr=max(mr,0);yr=max(yr,0);
	int maxi=999999999;
	int mt = mr==0?999999:mh/mr +(mh%mr>0);
	int yt = yr==0?999999:yh/yr +(yh%yr>0);
	if(mt<yt){
		cout<<"0"<<endl;
		return 0;
	}
	if(mt==999999999 && yt==999999999){
		cout<<ca*(md-ya+1)<<endl;
		return 0;
	}
	maxi=999999999;
	// increase hp
	maxi=min(maxi,((mt-yt+1)*yr)*ch);
	//atk
	cout<<yt<<endl;
	int incatk =  ((mh / (yt+1) )+ (mh % (yt+1)>0 ) ) -ya;
	maxi=min(maxi,incatk*ca);
	//int indef = 
	cout<<maxi<<endl;
	
}