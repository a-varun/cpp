#include <iostream>
#include <vector>
#define ll long long
using namespace std;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		ll chars[26]={0};
		string s;
		cin>>s;
		ll p,q,temp,fl;
		cin>>p>>q;
		for(int i=0;i<s.length();i++)
			 chars[s[i]-'a']++;
		for(int i=0;i<26;i++)
			 chars[i]*=p;
		for(ll i=0;i<q;i++){
			cin>>temp;
			fl=0;
			for(int i=0;i<26;i++){
				temp-=chars[i];
				if(temp<=0) {cout<<char(i+'a')<<endl;fl=1;
				break;}
			}
			if(!fl) cout<<"-1"<<endl;
		}
	}
}