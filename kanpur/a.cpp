#include <iostream>
#include <vector>
#define ll long long
using namespace std;

int main(){
	int ite;
	cin>>ite;
	while(ite--){
		ll s,e;
		cin>>s>>e;
		int n;
		cin>>n;
		ll t1,t2;
		int fl=1;
		for(int i=0;i<n;i++){
			cin>>t1>>t2;
			if(t1>e || t2<=s)
				continue;
			fl=0;
		}
		if(fl) cout<<"YES"<<endl;
		else
			cout<<"NO"<<endl;

	}
}