#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>

using namespace std;

#define REP(i, n) for(long long int i=0; i<(n); i++)
#define FOR(i, a, b) for(long long int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(long long int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(long long int i=(a); i<(b); i+=(c))

#define SS ({long long int x;scanf("%d", &x);x;})
#define SI(x) ((long long int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<long long int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<long long int, long long int> PII;
typedef long long llong;
const int MAXN = 1000009;
llong tree[MAXN], ar[MAXN], B[MAXN];
llong read(int idx){
	llong sum = 0;
	while (idx > 0){
		sum += tree[idx];
		idx -= (idx & -idx);
	}
	return sum;
}
void update(int idx ,llong val){
	while (idx <= MAXN){
		tree[idx] += val;
		idx += (idx & -idx);
	}
}
int main() {
	long long ite,k,ptr;
	cin>>ite;
	for(int rs=1;rs<=ite;rs++){
		cout<<"Case "<<rs<<": ";
		long long n,a,inv_count=0;
		cin>>n>>k;
		memset(tree, 0, sizeof(tree));
		for(int i = 1; i <=n; ++i) {
			cin>>ar[i];
		}
		for(int i = n ; i >= 1; --i) {
			llong x = read(ar[i] - 1);
			inv_count += x;
			update(ar[i], 1);
		}
		if(k<inv_count)
			cout<<inv_count-k<<endl;
		else{
			if((k-inv_count)%2==0)
				cout<<"0"<<endl;
			else
				cout<<"1"<<endl;
		}
	}
	return 0;
}
