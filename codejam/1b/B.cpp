#include <bits/stdc++.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main(){
	int ite;

	cin>>ite;
	int o=1;
	while(ite--){
		cout<<"Case #"<<o++<<": ";
		int r,c;
		cin>>r>>c;
		int ar[r][c];
		memset(ar,0,sizeof(ar));
		int n;
		cin>>n;
		while(n--){
			for(int i=0;i<r;i++){ for(int j=0;j<c;j++) cout<<ar[i][j]<<' ';cout<<endl;}
				getchar();
			int tx = -1, ty=-1, tc = 999999;
			for(int i=0;i<r;i++){
				for(int j=0;j<c;j++){
					if(ar[i][j]==0){
						int tsum = 0;
						if(i>0) tsum += ar[i-1][j];
						if(i<r-1) tsum += ar[i+1][j];
						if(j>0) tsum += ar[i][j-1];
						if(j<c-1) tsum += ar[i][j+1];
						//if(n==0) cout<<i<<' '<<j<<' '<<tsum<<endl;
						if(tsum < tc){
							tc = tsum;
							tx = i;
							ty = j;
						} 
					}
				}
			}
			ar[tx][ty]=1;
		}
		for(int i=0;i<r;i++){ for(int j=0;j<c;j++) cout<<ar[i][j]<<' ';cout<<endl;}
		int ans = 0;
		for(int i=0;i<r;i++){
				for(int j=0;j<c;j++){
					if(ar[i][j]==1){
						int tsum = 0;
						if(i>0) tsum += ar[i-1][j];
						if(i<r-1) tsum += ar[i+1][j];
						if(j>0) tsum += ar[i][j-1];
						if(j<c-1) tsum += ar[i][j+1];
						ans += tsum;
					}
				}
			}
			cout<<ans/2<<endl;
	}	
}