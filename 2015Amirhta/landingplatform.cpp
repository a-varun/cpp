#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define INF 1000000000
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;
#define MIN -99999999999
LL ar[101][101];
int n,m;
LL maxi;
long long int isvalid(PII a){
	int x=a.FST,y=a.SEC;
	if(x<0||x>=n||y<0||y>=m) return 1;
	return 0;
}

LL retpt(PII a){
	int x=a.FST,y=a.SEC;
	return ar[x][y];
}

PII inc(PII a, int type){
	int ar[][2]={{-1,-1},{-1,1},{1,-1},{1,1}};
	int x=a.FST,y=a.SEC;
	type--;
	return make_pair(x+ar[type][0],y+ar[type][1]);
}

long long func(PII lu, PII ru, PII ld, PII rd,LL ap){
	LL sum=MIN;
	while(1){
		if(isvalid(lu)||isvalid(ru)||isvalid(ld)||isvalid(rd)){
			break;
		}
		if(sum==MIN) sum=0;
		sum+=retpt(lu)+retpt(ru)+retpt(ld)+retpt(rd);
		maxi=max(maxi,sum+ap);
		lu = inc(lu,1);
		ru = inc(ru,2);
		ld = inc(ld,3);
		rd= inc(rd,4);
	}
	return sum;
}

int main(){
	int ite;
	scanf("%d",&ite);
	while(ite--){
		memset(ar,0,sizeof(ar));
		cin>>n>>m;
		
		maxi=MIN;
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				cin>>ar[i][j];
			}
		}
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
					LL k=func(make_pair(i-1,j-1), make_pair(i-1,j+1), make_pair(i+1,j-1),make_pair(i+1,j+1),ar[i][j]);
					if(i!=n-1&&j!=m-1){
						k=func(make_pair(i,j), make_pair(i,j+1), make_pair(i+1,j),make_pair(i+1,j+1),0);
					}
			}
		}
		cout<<maxi<<endl;
	}
}