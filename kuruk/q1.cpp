#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <ctype.h>
#include <bitset>
#include <assert.h>
#include <string.h>
 
using namespace std;
 
#define REP(i, n) for(int i=0; i<(n); i++)
#define FOR(i, a, b) for(int i=(a); i<(b); i++)
#define IFOR(i, a, b) for(int i=(a); i>=(b); i--)
#define FORD(i, a, b, c) for(int i=(a); i<(b); i+=(c))
 
#define SS ({int x;scanf("%d", &x);x;})
#define SI(x) ((int)x.size())
#define TCases int ite;cin>>ite;while(ite--)
#define PB(x) push_back(x)
#define MP(a,b) make_pair(a, b)
#define SORT(a) sort(a.begin(),a.end())
#define ITER(it,a) for(typeof(a.begin()) it = a.begin(); it!=a.end(); it++)
#define ALL(a) a.begin(),a.end()
#define V vector
#define S string
#define FST first
#define SEC second
typedef V<int> VI;
typedef V<S> VS;
typedef long long LL;
typedef pair<int, int> PII;

int main()
{
	int ite;
	scanf("%d",&ite);
	while(ite--)
	{
		int t1,t2,n,m,dx,dy;
		scanf("%d%d%d%d%d%d",&n,&m,&t1,&t2,&dx,&dy);
		int vis[n][m];
		dx--;dy--;
		t1-=1;t2-=1;
		vector<string> ar;
		string st;
		for(int i=0;i<n;i++)
		{
			cin>>st;
			ar.PB(st);
		}
		stack<int> x;
		stack<int> y;
		x.push(t1);
		y.push(t2);
		int fl=0;
		while(!x.empty())
		{

			int tx=x.top();
			int ty=y.top();
			x.pop();
			y.pop();
			cout<<tx<<'\t'<<ty<<endl;
			if(tx<0||ty<0||tx>=n||ty>=m) continue;
			if(vis[tx][ty]) continue;
			if(ar[tx][ty]=='R')
			{
				ar[tx][ty]='G';
				continue;
			}
            vis[tx][ty]=1;
			if(tx==dx&&ty==dy) {fl=1;break;}
			x.push(tx-1);
			x.push(tx+1);
			x.push(tx);
			x.push(tx);
			y.push(ty);
			y.push(ty);
			y.push(ty-1);
			y.push(ty+1);


		}
		if(fl)
		{
			cout<<"Yes"<<endl;
		}
		else
			cout<<"No"<<endl;

	}
}